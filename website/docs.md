# Docs introduction
**Welcome to the documentations of Delibay !** Documenting everything needed is an iterative process along the development and evolution of Delibay and is currently very beta.

Depending on what you are searching for, you can use one of the parts on the left menu.

## Progress
| Guide                  | State | Comments                                                                            |
| ---------------------- | ----- | ----------------------------------------------------------------------------------- |
| **User guide**         | 50%   | There is already a good base, mostly around transcripts.                            |
| **Concepts**           | N/A   | Features are constantly brainstormed and feedback are requested.                    |
| **Developer guide**    | 15%   | Irregular ongoing process to enable code contributions in the future. 30% are done. |
| **Contribution guide** | 5%    | Just the intro to indicate contributions are not opened for the moment.             |

