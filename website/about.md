<!-- omit from toc -->
# About

- [About the creator](#about-the-creator)
- [About the project](#about-the-project)
- [Meta about](#meta-about)
- [Credits](#credits)

## About the creator

**Hi, I'm Samuel Roland, an IT student (third class of a 3 years-long Bachelor). I'm passionate about programming and Free software, and want to live out of producing Free and ethical software.** This project is obviously influenced by my perspective on school and teaching. I appreciate teaching things to others and learning things out of school.  
With the fantastic book *Peak* by Anders Ericsson and Robert Pool, I discovered and was fascinated by deliberate practice during summer holidays in 2022. After a little retrospective on my last studies in IT and mandatory school, I believe we all missed a great deal of practice and feedback opportunities. For me, deliberate practice has a huge potential to increase the effectiveness of learning and the engagement of students.

For the context, at my current undergraduate school, we are using Moodle to host files theory/exercises/solutions documents in PDF and slides. We use Moodle to release practical works each week. Microsoft Teams is used a bit for informal chatting (email in other cases). We host our code on GitHub, and we use GitHub Classroom to create repositories for each project. If we are lucky we get code reviews by the teacher via a pull request, if not we just get a grade and a grid of points. Some teachers have started to use Socrative to host exercises and do live sessions, the idea is good but Socrative has some major UX issues and doesn't focus on the students...

**Retrospective on school years**  
Looking back at my precedent and current studies, I was frustrated by the big lack of feedback during my school years, especially in my last 4 years learning IT in a professional school. Furthermore, I wish I had regular code reviews of exercises, regular advices on how to write better code, how to write tests (a great way to have fast feedback too), and specific advices on writing good documentations. I'm happy with projects and practice we made, some of them were very interesting, but I regret all the theory periods, where we had only minimal practice.

Before I discovered the term "mental representation" which is a central concept of deliberate practice, I was already trying to have a mental view of programming concepts such as classes, objects, loops, switch, etc... Metaphors, characteristics, and trials, helped me to understand. Explaining things to my colleagues forced me to clarify my mental view to be able to define concepts with precise sentences and appropriate metaphors. If teachers were more often focused on practice, explaining stories and metaphors to link abstract concepts to the real world, this would increase the chance that students build a strong mental representation of complex concepts, and thus improving their skills.

**But don't get me wrong, even if I have a lot of opinions on how IT teaching should evolve, I like my past and current studies! I just believe that there is a lot to gain by adopting deliberate practice and a dedicated tool, to optimize the learning and teaching process.**

## About the project
A few weeks after the start of my bachelor, I felt the need to have a dedicated application to train intensively on different skills. During a course about logical systems, I thought it would be nice to have a small training app to learn to get fast at numbers conversion (from and to binary, decimals, or hexadecimals). I knew how to do it, but I was slow and having a dedicated training would help me get better.  
In other courses like the C++ programming course, I felt that even if I understood most programming concept, I really needed to practice every detail to make sure I understand them fully. To be ready for evaluations, it was important to have seen edge cases, and to be able to build other skills on top of base ones. During training for the first evaluation, I read about 300 slides of theory, I saw that some of the details to remember were not put into practice in exercise or project. As I already did 4 years of IT learning, I knew a part of the basics, but there was another part of new things distributed among the slides. At this moment I wish I had a clear view of which skills should be trained and a way to specifically train them, but I had no other way than reading everything...

The project began at the end of September 2022 in my head and by brainstorming it with a few classmates. I slowly wrote down the design document to structure my thoughts and present the project. `delibay.org` was deployed at the end of December 2022, the first mature version of the design document was released, and then I started developing the MVP...

The project slowly progressed during 2 years of small iterations, mostly during holidays. I met with a dozen of teachers to present my project. I did a lot of refactoring, migrated a few tools, rewrote the DY parser, changed the syntax multiple times... The last blocking piece is the deployment system, mostly the update system which is harder than expected.

There hasn't been progress in the project since May 2024, my focus has shifted for other projects like [PRJS](https://github.com/samuelroland/prjs), [PLX](https://plx.rs), [automating my Fedora setup with Ansible](https://codeberg.org/samuelroland/setup), [writing cheatsheets for productivity tools](https://codeberg.org/samuelroland/productivity/), learning Fish, switching to Neovim, learning Rust... The project is in break currently, I'm excited to continue it when I can find time, but that's probably not before the end of my Bachelor (August 2025).

<!-- TODO: add screenshot of the first POC -->

## Meta about
This website is built with Laravel, CommonMark PHP parser, TailwindCSS, the typography plugin and a few chocolate bars :)
If you have seen a typo, or you want to suggest changes, please open an Issue or even a PR [on the repository delibay/delibay.org](https://codeberg.org/delibay/delibay.org) !!
The whole infrastructure is hosted on a VPS on renewable energy, by [Infomaniak](https://www.infomaniak.com).

## Credits
<!-- Credits are for delibay.org, not this 'project' repository! -->
delibay.org is a free software released under the [AGPL-3.0-or-later](https://www.gnu.org/licenses/agpl-3.0.html).

	delibay.org - Presentation website of the Delibay project
	Copyright (C) 2022-present Samuel Roland

	This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

- [Laravel](https://github.com/laravel/framework) - MIT - Copyright (c) Taylor Otwell
- [TailwindCSS](https://github.com/tailwindlabs/tailwindcss/) - MIT - Copyright (c) Tailwind Labs, Inc.
- [TailwindCSS Typography plugin](https://github.com/tailwindlabs/tailwindcss-typography/) - MIT - Copyright (c) Tailwind Labs, Inc.
- [thephpleague/commonmark](https://github.com/thephpleague/commonmark) - BSD 3-Clause License - Copyright (c) 2014-2022, Colin O'Dell

