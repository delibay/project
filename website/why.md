## Why Delibay ?

**I believe that any motivated IT student should have the possibility to train at a high level to acquire complex IT skills, and get the most out of school courses. Theory courses, practical works, and homework are often not designed to provide an effective practice. This makes harder to get solid skills, especially for students who are struggling. In my experience, a lot of teachers don’t give regular and specific feedback on students’ work. The rare feedback are grades or comments on tests answers. I think students don’t have the opportunity to constantly improve the way they code, think, and solve problems, and it slows down the progression.**  
**As an important side effect of better learning, motivated students can learn on their own, in other programming languages and contexts, and build or contribute to bigger projects.**

Theory courses are necessary but often too long and focused on knowledge instead of skills. Students engagement and interest would be higher, by showing concrete demonstrations and thinking in action to solve concrete problems instead of long presentations. A lot of students can’t concentrate such a long time at listening.

I usually revise my tests a few days before, trying to read theoretical material and doing old tests. When I read several hundreds of slides and dozens of notes, it took me a lot of time, I wasn't sure I had the taught skills, and if I had a good understanding that would let me adapt to edge cases or more challenging situations. As there is no precise measure of progression and mastery of each concept, students can't really identify precisely their weaknesses that need the most practice and have no easy way to train them intensely.

Among IT skills, programming is more than just making a piece of code work; code quality, architecture, refactoring, and good practices are other parts where teacher's feedback are really necessary but often nonexistent.

Exercises made for homework or during classes are not automatically corrected, and when students don’t have access to solutions it can take days before they can verify their answers. PDF is not the right format to store exercises or responses, because there is no possible interaction and automatic correction. There is no easy way for teachers to review dozens of exercises without a good tool.

Students sometimes struggle during several weeks before getting a clear understanding of a programming concept. This problem of mental understanding depends on the investment of the student, and the teaching skills of teachers. When visuals, metaphors, or abstractions are missing, Delibay can’t help much on this directly. But it can help the teacher along the process to measure students' skills, asking questions to everyone, and finally providing feedback to build better mental representations.

Exercises instructions, school documentations or other resources are sometimes confusing or unclear. Exercises may not cover the whole spectrum of details required for a skill. As students can't contribute to the course, and teachers don't want or don't have time to enhance it, these problems can exist for years...

## How ? 
**To have an effective practice, teachers and students need to adopt *deliberate practice*, the gold standard for experts and champions in any field. Top level violinists, expert chess players, best golf players, … use this uncommon kind of practice to take advantage of the brain and body adaptability.**  
**Students’ aim is not necessarily to become experts, however there is a lot to learn from deliberate practice to accelerate students’ progression by building better practice driven courses.**

Courses should be divided in skills not topics. Theory should be intercalated with very short exercises. After this, the teacher knows whether students got the concepts and have good mental images. I believe practicing each concept, even quickly, help a lot to memorize, visualize and understand what is being taught, and increases the long term retention of information.

Students engage in regular training on specific exercises at home or in class. They identify weaknesses with hard exercises (failed exercises and/or low level of confidence), working on them, reading teacher's feedback, fixing errors by iteration, ... Having a custom training (with a set of hard exercises) gives a plan to revise before tests.

As all exercises are stored in the platform, automated feedback is possible: automated correction enables an instant feedback loop which help to fix mistakes as early as possible. For projects and exercises where the answer must be judged by a human, delibay makes it easy to ask for feedback, and as a teacher, to give feedback on the students' answers. Feedback could be given by other classmates too, when time lacks for teachers or inside teams.

As a teacher, asking *everything is okay, do you have any question?*, is not enough to know what students know and understand. There is no warranty that confused students will raise their hands to ask for help. Doing small concrete exercises to try to apply the knowledge, will provide to teachers a precise report of skills of each student in the class.

Students should be able to contribute to the course, by giving feedback on the content of exercises and resources. They can also directly propose changes or create new exercises to cover other cases and after teacher approval, everyone in the school can use it.

## What ?
**Concretely Delibay is a centralized platform for learning with deliberate practice principles, including a web application and a central server.** They have an account and are part of a school. Students use Delibay during classes and outside, doing trainings at home, live exercises in class, asking for feedback, working on projects, and completing exercises.  
Teachers manage classes and courses, divide courses in skills and sub-skills with clear goals, and create exercises and resources for taught skills. They give feedback in a series on students' answers, review students contributions, and look at the general progress of skills mastery in the class.

**Delibay is an open platform**:
- free as in freedom: all source code and documentations are released under a free license *(not yet actually but coming soon I hope)*, making it possible to change the server and the client to your needs
- built around an API: enables teachers and student to create automations for specific needs
- an open project: discussions and contributions are welcome to enhance the experience and features of Delibay. The project need feedback too (not only students)!

