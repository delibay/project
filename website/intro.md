<style>
	.heading-permalink {
		display: none;
	}
</style>

<div class="my-10">
	<div class="text-[#21aa78] italic p-2 px-3 md:px-6
	font-semibold text-2xl sm:text-3xl md:text-4xl
	mx-1 md:mx-3 my-3 md:my-5 border-l-2 border-[#20a8c3]">
	Redefine the IT learning experience with deliberate practice
	</div>
	<div class="italic p-2 px-3 md:px-6
	font-semibold text-lg sm:text-xl md:text-xl lg:text-2xl
	mx-1 md:mx-3 my-3 md:my-5 border-l-2 border-black">
		Empower IT students to train intensively on IT skills to develop efficient mental representations<br>
		Help IT teachers to design practice-oriented courses and give feedback at scale
	</div>
</div>

## The learning experience is broken
Based on recent education research, **long lecture-based courses are not efficient at deeply learning skills**. Students have rare occasions to **practice concepts by themselves** in class and **rarely receive specific feedbacks** to allow continuous improvement. Deliberate practice states that active learning through intense practice with frequent feedback can build efficient mental representations, finally leading to expertise. 

Students doesn't necessarily aim to become experts in a given IT field, but there is a huge potential to learn from the various research on expertise to build practice driven courses. **This is why students and teachers need to adopt *deliberate practice* !**

## What's really deliberate practice ?
<div class="">

<!-- The initial problem is that *the amount of experience and practice is not correlated to amount of expertise*.  -->

Delibay is based on the science of deliberate practice, **a specialized type of practice used by top level experts** all around the world to achieve extraordinary performances. These principles are designed to get the most out from the brain and body adaptability.

</div>

<div class="flex flex-wrap font-bold text-center">
<div class="m-2 ">
	<img class="max-w-xs h-min" src="/imgs/dp-training.svg" />
	<span>Intense and challenging training</span>
</div>
<div class="m-2 items-center">
	<img class="max-w-xs h-min" src="/imgs/dp-feedback.svg" />
	<span>Continuous feedback and adjustment</span>
</div>
<div class="m-2">
	<img class="max-w-xs h-min" src="/imgs/dp-goals.svg" />
	<span>Specific goals</span>
</div>
<div class="m-2">
	<img class="max-w-xs h-min" src="/imgs/dp-mentalrep.svg" />
	<span>Building efficient mental representations</span>
</div>
</div>

A relevant quote from the book *Peak, How all of us can achieve extraordinary things* by Anders Ericsson and Robert Pool:
> What exactly is being changed in the brain with deliberate practice? The main thing that sets experts apart from the rest of us is that their years of practice have changed the neural circuitry in their brains to produce highly specialized mental representations, which in turn make possible the incredible memory, pattern recognition, problem solving, and other sorts of advanced abilities needed to excel in their particular specialties. <br> [Page 63 - Peak, by Anders Ericsson and Robert Pool]


## Applied to IT learning
The challenge now is to take the deliberate practice principles and find ways to apply them to the IT tools, context and challenges. IT teachers wanting to apply deliberate practice in their courses will need a specific tool to create training opportunities and get a clear vision on their class understanding. Viewing students answers and give back feedback is not possible via e-mail...

One difficulty of learning programming is to be able to mentally fully visualize the program flow, how control flows impacts execution, how concurrent operations operate, how errors could happen, what is the algorithmic complexity, what are the most appropriate data structures... This can only be deeply learned with tons practice and short feedback on examples, comparisons, trade-offs analysis by students themselves. Trying to run a program is its head with questions like "What is the output of this program ?" really help to get the basics, asking teachers needs to ask tricky questions to progressively build a precise mental model. Delibay supports autocorrected textual, boolean and MCQ questions. It will be able to support filling tables too.

Teachers do not have a clear understanding nor precise metrics of the class' understanding. Asking questions and seeing answers, allows to discover misunderstandings quickly and to know if they were addressed efficiently. These exos can be answered in class and the teachers will be able to see them and give feedback in live. Explanations associated with solutions will also help students train alone and better understand their errors.

The easiest way to shorten the feedback loop is to write automated tests on top of programming exos, but then you need a dedicated test runner. This is why we also develop [PLX](https://plx.rs).

<!-- 1. Intense and challenging training
1. Delibay provides tool to create and train on different exo types, far from MCQ
1. Continuous feedback and adjustment
1. Specific goals
1. Building efficient mental representations  -->

## Building deliberate courses
We want to help teachers in the design or redesign of their courses, based on what we learned from DP. By providing examples and practical advices, more coming soon...

<!--TODO: show exo formats...-->
<!--TODO: show priority mode...-->

## A deliberate deployment strategy

<div class="">

As a teacher, are you tired of the *Word -> PDF -> Upload -> Inform about update* process to ship your exercises to your students ? Bored with editing dozens of course contents in various forms ? Annoyed to manage solutions in separated files ?

![A standard deployment process: Word document for exos and solutions, export to PDF, upload to a drive somewhere and inform students about updates](/imgs/default-deploy.svg)

How much time you spend in this process ? 5-10 minutes ? And how much does it slow down frequent updates ?

**Delibay takes another strategy: manage the course with text files outside of Delibay in a public Git repository.**

Once configured, deploying a course takes **under 10 seconds**. You can use your preferred IDE where you already feel productive, and the deployment is a `git push` away.

</div>

Let's say you have a course on regexes, you have a Git repository named `DRX`, here is the deployment process.

<div class="">

![deploy-strategy.svg](/imgs/dp-deploy-strategy.svg)

</div>

This fast deployment strategy allows a course to have very frequent improvements, to adapt to student's feedback and to continue to enhance training opportunities. This makes collaboration between teachers and contribution a breeze to manage (via Pull requests).

### An easy syntax - DY
<!-- and great editing experience -->
In your Git repository, you can define your course information and your exercises with human-readable text files using an intuitive syntax called the DY syntax.
```dy
Exo: Who is the creator of the Linux kernel ?
Solution: Linus Torvalds

Exo: Introduction
Which statements are correct ?

Options: [multiple]
- C is an interpreted language
- [ok] C is a compiled language
- [ok] C is used in the Linux kernel
- C is mostly used for web applications
```

More to come related to the DY syntax...
