---
order: 1
---
# Getting started

## Using in beta
Delibay doesn't have a production release, we are far from being able to tag the `v1.0.0`. Using a beta software means they are bugs, some of them will not be fixed before weeks if they don't cause a lot of issues. Everything is moving pretty slowly as I'm the single person working on this project, and I'm pretty busy with school projects and homework. I try to move the project forward every week but the amount of effort and outcome is irregular.

The interface, workflows, DY syntax and other defined concepts will continue to change iteratively, don't be surprised to see things move around, being added or removed, but please contribute to the conception by giving feedback, this is the only way to make the project perfectly adapted to your needs.

## Prerequisites
<!-- Internet -> at least to download content... //enable this when assets are cached -->
1. A browser + internet connection
1. A Delibay server somewhere, you should have the URL of this server where you can be granted access. If you just want to try it out, there is a demo server.
1. To be okay to use unstable beta versions

## Progress
**Overview of the implemented features**:
1. Register, login and logout
1. Courses and skills browsing inside the organisation
1. Training on basic exo formats, where answers are saved:
   1. Simple text exo
   1. True/False exo
   1. Multiple Choice Question (MCQ) exo
1. Advanced exo formats
   1. Table exo (half working)
1. Priority mode in training
1. Syncing answers up towards the server, syncing down new or updated courses, skills or exos
1. Parsing code for transcript import (80% done)

**Features currently developed**:
1. Table format
1. Import interface
1. Update system and interface
1. Open exo format
1. Fix exo format
1. A lot of documentation and some features on the website
1. An experimental extension to support the DY syntax in VSCode
