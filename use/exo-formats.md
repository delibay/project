---
order: 7
---
<!-- omit from toc -->
# Exo formats
An overview of all possible exo formats and their configurations.

**Warning: the following formats are not fully documented yet... and some images are missing.**

- [Exo definition](#exo-definition)
- [Basic formats](#basic-formats)
	- [Text exo](#text-exo)
	- [Open exo](#open-exo)
	- [Boolean exo](#boolean-exo)
	- [MCQ exo](#mcq-exo)
- [Advanced formats](#advanced-formats)
	- [Table exo](#table-exo)
	- [Code fixing](#code-fixing)

## Exo definition
| Property | Prefix         | Comment                                                                                                                                                                                |
| -------- | -------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Name     | `Exo:`         | This prefix defines the start of the exo block. If the exo has subexos, it is a parent exo, therefore it cannot be filled (using a `Solution:` prefix will throw an error).            |
| Code     | ` `            | The instruction is an empty prefix, starting just below the `Exo:` or `Subexo:` and ends before another prefix is found. It supports multiline Markdown.                               |
| Code     | `Options:`     | Define a list of options for MCQ (See more below).                                                                                                                                     |
| Code     | `Solution:`    | Defines the solution in list mode or single line style. Mandatory except for MCQ and table formats                                                                                     |
| Code     | `Explanation:` | Like instructions, explanations do support Markdown on multiple lines. It is displayed once the exo is done.                                                                           |
| Name     | `Subexo:`      | This prefix defines the start of the subexo block and can only be defined under a parent exo. A subexo has the same available sub prefixes defined above (Except `Exo:` and `Subexo:`) |

<!-- Add tables... -->
<!-- Add some hierary in this table ?? -->
<!-- Should we really support a Source info at this level ???? -->
<!-- | Code     | `Source:`  | Define a list of options for MCQ (See more below).                                            | -->
Notes:
1. Notice that you don't need to explicitly specify the exo format, the default one is Text format and other formats are deduced from the context (mostly which prefixes you used).
1. *Reminder: the prefixes are correctly extracted when given in correct order (see the order of above rows), and only below a `Exo:` prefix.*

<!-- TODO: how parent explanations actually works ? -->

## Basic formats
Standard formats you would expect.

### Text exo
This is the easiest exo format, with a text input to answer something in text.

```dy
Exo: Who is the creator of the Linux kernel ?
Solution: Linus Torvalds
```

Another example with a multiline instruction (automatically detected between `Exo:` line and `Solution:` line):
```dy
Exo: Optional parameter
Declare a function `acronym` that can be called with 1 or 2 parameters (`firstname` and optionally `lastname`). The default value of the second parameter is `""`.
~~~php
//Examples of calls
acronym("Lucie", "Lounin");
acronym("James");
~~~
Solution: function acronym($firstname, $lastname = "")
```

We define one exo with title `Optional parameter` and the instruction starts at `Define a function...` and ends at the end of the code snippet (because it founds another prefix, here `Solution:`).

### Open exo
**Warning: this is NOT implemented and the DY syntax might evolve a bit.**

Open exo is a variant of text exo where the solution cannot be automatically corrected by Delibay because this is too long and subjective. This is typically useful for open questions (when you ask students to justify or explain things) and small coding exercise (can be done in a single file).

To designate an open exo, you add the `[open]` keyword on the `Exo:` prefix. The solution is more like an explanation and will just be displayed when the student's answer has been submitted.

```dy
Exo: [open] Method to convert decimal to binary
Can you explain step by step how to transform the decimal value `1000` in binary ?

Solution:
We need to get the **modulo of 2** of the value and **do an integer division by 2** at each round. Once we reach 0 after the division by 2, we stop the process. Then we just take the list of results for modulos **in the reverse order** and we have our result !
1. 1000/2 = 500 and 1000 % 2 = 0
1. 500/2 = 250 and 500 % 2 = 0
1. 250/2 = 125 and 250 % 2 = 0
1. 125/2 = 62 and 125 % 2 = 1
1. 62/2 = 31 and 62 % 2 = 0
1. 31/2 = 15 and 31 % 2 = 1
1. 15/2 = 7 and 15 % 2 = 1
1. 7/2 = 3 and 7 % 2 = 1
1. 3/2 = 1 and 3 % 2 = 1
1. 1/2 = 0 and 1 % 2 = 1

**Finally we have `11'1110'1000`**
```

Note: the workflow on student's side is not very clear/defined for the moment, especially for exos where the teacher is not going to give feedback. 
- Do you have any idea on this ? 
- How do students decide if they are at ease with the related concepts or not, and whether they would like to train it later again ? 
- (The start of the problem is that answers are not assigned as correct or incorrect, this is more obviously nuanced and we have no way to express that now...)

<!-- TODO: document how open keyword applies to childrens -->
<!-- TODO: does explanation are really useful in this context ? should they be banned ? -->


<!-- TODO: add 2 images here -->

<!-- Multiline mode ? -->

### Boolean exo
2 examples of the standard True/False question. Similar to MCQ in display.

<!-- TODO: add an image here -->
```dy
Exo: C is an interpreted language ?
Solution: false
```

```dy
Exo: C files are firstly compiled to .o files before being linked ?
Solution: true
```

If the solution is `true` or `false`, this automatically makes it a boolean exo. There is no need to define the list of options, this is always `true` or `false`. During the training the maximum trials number is ignored in this format because once we got it wrong there is no sense to try again...

### MCQ exo
Multiple Choice Question can be used in 2 modes:
- Standard: only one option can be chosen.
- Multiple: multiple options can be checked. All correct options should be checked to be judged as correct.

To indicate the correct options, you add the `[ok]` keyword before the option text. In this way, there is no Solution block to provide.

<!-- TODO: add 3 images here with and without multiple mode, colors on results. -->
**Standard mode:**
```dy
Exo: Guess the sorting algorithm
The following list `[4, 2, 7, 2, 9, 5, 4, 3, 3]` has been sorted with different 
sorting algorithms and stopped before the end of the sort.  
Guess which algorithm was used in each case.

Subexo:
**`[2, 2, 3, 3, 4, 5, 4, 3, 9]`**
Options:
- Bubble sort
- [ok] Insertion sort
- Merge sort
- Quick sort

Explanation: you can see elements that have been swapped between the start and the end of elements.
```

**Multiple mode:**
To set exo in multiple mode, you add the `[multiple]` keyword on the Options block
```dy
Exo: Warm up
Which statements are correct ?

Options: [multiple]
- C is an interpreted language
- [ok] C is a compiled language
- [ok] C is used in the Linux kernel
- C is mostly used for web applications
```

## Advanced formats
These formats are more related to IT learning and more powerful than basic formats. They usually contains more than one value to enter.

### Table exo
**Warning: this is starting to be mature, but this is not implemented for now and the DY syntax might evolve a bit. For now, this section is left because this shows how you could do it in the future.**
<!-- That's a more complex exo type, and it should be used only when there is no other possible formats, and line of the table cannot be divided in subexos with only a 1 or 2 values to give.  -->

Filling a big table of values and doing manual comparison to the solution takes a lot of mental energy, so we invented the Table format to provide regular feedback on each line, letting you focus on the exo not the correction. This is not comparable to Markdown tables (that can be used in instructions), because they can't be filled in. This is why there is a dedicated format.

*Note: In this table exo format, we are talking about a fillable table with text inputs and automatic feedbacks. Other tables that are not fillable and only for information purpose can continue to exist in instruction or explanation, they use the Markdown syntax not the one described below... Therefore fillable tables must have at least one fillable cell and associated solution.*

A first minimalist example. At left the third column should be filled by the student. At right the solution.

**The OR gate**  
*Fill the truth table of the OR logic gate*

<div class="flex">
<div class="w-32 mr-10 table">

Empty table:
| A   | B   | A+B |
| --- | --- | --- |
| 0   | 0   |     |
| 0   | 1   |     |
| 1   | 0   |     |
| 1   | 1   |     |

</div>

<div class="ml-10 w-32 table">

Solution table:
| A   | B   | A+B   |
| --- | --- | ----- |
| 0   | 0   | **0** |
| 0   | 1   | **1** |
| 1   | 0   | **1** |
| 1   | 1   | **1** |

</div>
</div>

We would write this:
```markdown
Exo: The OR gate
Fill the truth table of the OR logic gate

Table:
Header:
- A
- B
- A + B
Row:
- 0
- 0
- [text] 0
Row:
- 0
- 1
- [text] 1
Row:
- 1
- 0
- [text] 1
Row:
- 1
- 1
- [text] 1
```

**Explanations:**
1. You start a table with `Table:`
1. You can define 0, 1 or more header row with `Header:` per row.
1. Then you can insert 1 or more body rows with `Row:`
1. You define cells in each row with a simple unordered list
1. To indicate a cell with a given text, just give the text
1. To indicate a fillable cell, use `[text]` and right after the solution.

**Rendering**  
Delibay will render *empty* text inputs for each `[text]` cell (here on the last column). Header rows will be displayed in bold.

**Feedback**  
Currently there is a feedback on each filled cell when you leave the cell (the background of the cell becomes green or red). This might change to a line-by-line feedback that might be more appropriate but this is still in brainstorming.

**Another complex table with none cells, empty cells and cells ranges:**

**Memory content**  
*Enter values in this memory after the previous assembly code:*
<div class="flex flex-wrap space-0 md:space-x-8 lg:space-x-12">

<div class="w-64 table">

The empty table provided:
|       | 1   | 2   | 3   | 4   | 5   | 6   | 7   | 8   |
| ----- | --- | --- | --- | --- | --- | --- | --- | --- |
| 0x100 |     |     |     |     |     |     |     |     |
| 0x108 |     |     |     |     |     |     |     |     |
| 0x110 |     |     |     |     |     |     |     |     |
| 0x118 |     |     |     |     |     |     |     |     |

Imagine some text inputs in all cells (except the top left one)
</div>

<div class="w-64 table">

The solution table:
|       | 1   | 2   | 3   | 4   | 5   | 6     | 7   | 8   |
| ----- | --- | --- | --- | --- | --- | ----- | --- | --- |
| 0x100 |     | 23  | 92  | A4  | 12  |       |     |     |
| 0x108 |     |     |     |     |     |       |     |     |
| 0x110 |     |     |     |     |     | 14/15 |     |     |
| 0x118 |     |     |     |     |     | 9/09  |     |     |

*`14/15` means in this example that 14 and 15 are both correct. (Same logic for `9/09`)*

</div>
</div>

The associated transcription:
```dy
Exo: Memory content
Instruction: Enter values in this memory after the previous assembly code:

Table: [height:5] [width:10]
Header:
- [none]
- [range:1...8]
Row:
- 0x100
- [empty]
- [text] 23
- [text] 92
- [text] A4
- [text] 12
Row:
- 0x108
Row:
- 0x110
- [empty:5]
- [text]
	- 14
	- 15
Row:
- 0x118
- [empty:5]
- [text]
	- 09
	- 9
```

**Explanations:**
1. `[empty]`: most of the cells are empty, meaning they have a text input but the solution is to leave them empty. **TODO: should they have another name ???**
1. `[empty:*]`: some consecutive empty cells
1. `[none]`: the top left corner is a none cell, there is nothing in it (no text input and so value to enter). **TODO: should they have another name like void cell ?**
1. `[none:*]`: some consecutive none cells
1. `[height:*]` and `[width:*]`: used to indicate the height and width of the table. This is optional but very useful if you want to let Delibay extrapolate with empty cells in case it's not defined.
1. `[range:*...*]`: it defines a list of cells with incremental values between 2 indexes. **TODO: `[range:*...*]` or `[range:*..*]` ??**
1. Define more solutions:  
   As defined in the core guidelines, you can write a list of values, as a sublist under `[text]`
	```dy
	- [text]
		- 09
		- 9
	```

**Important: all undefined cells in table body are interpreted as `[empty]`, and for header rows as `[none]`. The height and width is only useful to help with big tables to avoid defining empty cells at the end of rows.**

### Code fixing
This is exo format is will exist in the future but is still in brainstorming phase. Provide feedback on these reflections to help with the design of this feature. This is not a priority but we might develop the concept and define the DY syntax for this before we really implement it.

