---
order: 5
---
# DY syntax
The DY syntax is a simple human-readable and machine-parsable syntax to create an entire course in Delibay just with text files.

## Core principles
These are the core principles that rules the evolution of the syntax, this is useful to read this first before the details to understand the global logic defined for the syntax conception.

1. **Prefixes**: To indicate the property of an object. They consist of a single word started by a capital letter and an ending colon.  
   - Examples: `Course:`, `Exo:`, `Solution:`, `Table:`, ...
   - Some prefixes (`Course:`, `Skill:`, `Exo:`, `Subexo:`) marks the start of a block, in others words a multiline section for other prefixes related to the same object. For example there is no sense to use a `Solution:` prefix without a `Exo:` prefix before (you can't define a solution if you are not declaring an exo).
   - They must be placed at the very beginning of a line, or they will not be detected
   - Some prefixes are single line (only the text on the same line is considered), for titles i.e. Other prefixes are multiline and ends before the next prefix. Finally, there are list prefixes.
   - Prefixes must be given in correct order defined in each syntax documentation. In case two prefixes are mispositionned, the second one will be absorbed by the first one.
1. **Keywords**: Some prefixes can have settings/customizsations. They consist of a constant text, optionally followed by a colon and parameters. The whole is always written inside square brackets.
   - Examples: `[single]`, `[open]`, `[empty:3]`, `[range:3...10]`
   - They can be inserted only in certain places, generally after one or more specific keyword. In case we use several keywords the order doesn't matter.
   - In further examples when we refer to keywords with parameters, we often use the `*` symbol to indicate one parameter instead of choosing an arbitrary example. (Like `[range:*...*]`).
1. **Markdown support**: Instructions and explanations supports Markdown
   - Standard Markdown syntax is supported (our parser is [MarkedJS](https://github.com/markedjs/marked))
   - Code is highlighted
1. **List prefix structure**
	- There are 2 ways to define a list values (here with the exo `Solution` example):  
		*One item...*
		```
			Solution: bubble sort
		```
		*... vs multiple solutions:*
		```
			Solution: 
			- bubble sort
			- merge sort
		```
		Having a single element in the list mode is not an issue.
	- If your solutions contain dashes it's not an issue, it works the way you would expect it. A list dash is removed but further dashes are kept.
1. **Regular expressions** (regex) in solutions
   - Anywhere you have a text solution you can use two slashes to define a regex to validate instead of a regular text.
   - Example:
   	```
	Solution: /void f\(int +a, *int +b\);?/
	```
	**TODO: How to give an "example" solution to not show the regex which is hard to read ?**  
	**Warning: regex support is not implemented for now.**  
1. **Trimming** 
   - All values everywhere are [trimmed](https://en.wikipedia.org/wiki/Trimming_(computer_programming)) (whitespaces like spaces, tabs, end of line, are removed at the start and end of the text). If you put spaces after a solution value, they are removed as it is invisible.
   - If your exos contains solutions with spaces at the start or end, first consider to remove them. It will probably lead to more confusion with display and errors in student's input. But if it's really important to keep them, you have 2 options:
     - You wrap your solution with another character (for ex. `printf(" hey ")` -> `printf("| hey |")`)
     - You ask students (via the instruction) to replace it with another char like `_`

If you use prefixes or keywords in place where we don't expect them, they will be considered as normal text. 
<!-- As they have special highlighting colors it will be obvious to see when you misused them. -->

## Transcript example
Just as an example on how you can arrange several prefixes to declare 3 exos in a skill `Introduction`:
```dy
Skill: Introduction

Exo: C is an interpreted language ?
Solution: false

Exo: Linux creation
Subexo: Who is the creator of the Linux kernel ?
Solution: Linus Torvalds
Subexo: What is the year of the first Linux kernel version ?
Solution: 1991
```

Another example with a MCQ and 2 keywords `multiple` and `ok`. As you can notice, some prefix are empty (no `Instruction:` prefix), it is just positionnal (between `Exo:` and next prefix).
```
Exo: Guess the sorting algorithm
Initial state: `[4, 2, 7, 2, 9, 5, 4, 3, 3]`  
Intermediate state: **`[3, 2, 3, 4, 9, 5, 4, 7, 3]`**
Options: [multiple]
- Bubble sort
- Insertion sort
- [ok] Merge sort
- [ok] Quick sort
```

<!-- Exo: [open] How many lines has the Linux kernel ? Explain how do you count it.
Solution: TODO 
TODO and enable when open exo are implemented.
-->

## Syntax reference
For the moment you can discover and learn the details of the syntax in the 2 next pages. For instance, you will find examples for each exo formats. I'll probably document them here in a grouped way to provide a full reference in a single table.

## Better DY experience
Editing an uncoloured DY file is a bit unpleasant, this is why a VSCode extension is currently in experimental phase to make large course edition a breeze with the following features:
1. Syntax highlighting (colors on prefix and multiline content, colors on Markdown sections)
1. Live preview of exos, so you can directly see rendered exos instead of raw text.
1. Errors detections integrated in red underlining
1. Actions for easy setup

More to come on this project...

- TODO: how this vscode extension should be named ?
- TODO: is there some special/other features that would be helpful in this extension ?
