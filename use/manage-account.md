---
order: 2
---
# Manage your account

Delibay is currently only usable with an account. This is required to store answers, authenticate to access school courses, and be able to work on several devices.

## The server
When you create your account, you do it on a specific server, there is no single official server for everyone. If you visit the URL of your server, here we took `myschool.delibay.org` as an example, you should see this message.

![A Delibay server is running on this domain...](/imgs/use-delibay-server-running.svg)

As indicated, you need to go on [`app.delibay.org`](https://app.delibay.org) to login, as this is where the Delibay frontend is hosted.

## Register
The register process is pretty straightforward, you need to enter the server URL where you want to register. 
![Register start form](/imgs/use-register-start.svg)

Then you will need to read and accept the rules. This is usually short and important things to know and accept. If you don't want to accept the rules of your server, stop here and quit the page, previous information will be lost.
![Register rules](/imgs/use-register-rules.svg)

Then you just fill:
- a name
- an email, generally your professional email, so you can be automatically added to your organization
- a password and a confirmation

![Register user form](/imgs/use-register-form.svg)

When you are done, click `Create` and if everything is valid, you should be directly connected.

![Email validation message](/imgs/use-register-email.svg)
You can now verify your email address and access Delibay with the content of your organization !

## Login
To login, enter the server URL, your email and password, click on `Login` and you should be logged in.
![Login form](/imgs/use-login.svg)
<!-- TODO: fix integrated logo loading -->

## Logout
Clicking on the `Logout` button at the top right is enough to logout in this browser.

Actually this impacts 3 things:
- You are logged of the server (the server session is destroyed)
- All local data is removed (courses, skills, exos, answers, training temporary data, local only content).

<!-- TODO: implement this warning... -->
<!-- In case you still have some local content that has never been pushed, you will get a warning before logging out. -->

## Login back
This feature is not implemented but might be used to login again to the server in case the session has expired. The frontend might still show your data and account but you won't be able to do any server requests, this is problematic in case you had unsynchronized local answers.

In the meantime you sadly don't have another option that logout and login again, and you will lose your unsynchronized local data.

## Organization access
Courses are always managed under the umbrella of an organization, meaning you need to part of one to access some content. Currently, your organization could have configured that if your email address has been validated and is the organization's email, you can be automatically assigned to it.

When you join an organization, you have a role: teacher or student. The only difference currently is the possibility for teachers to create course and import content.

<!-- ## 2 FA -->
## Password change
This is not possible currently so ask help to your administrator.

## Account deletion
This is not possible currently so ask help to your administrator.

## Permissions
This is a matrix of permissions tied to your account, depending on your role on the organization and whether you can create/update a course as a teacher.

TODO this matrix