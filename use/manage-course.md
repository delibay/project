---
order: 3
---
# Manage a course
**We want to help teachers build practice-oriented courses. For this purpose, we build a new course management experience, to let you focus on designing and enhancing great deliberate courses in your specific IT domain.**

Let us first explain why the continuous effort in improving content and deploying modifications can be tedious with the traditional form-based or PDF approach. Then we'll talk about Delibay's approach.

## Problems of traditional content management
Here are a few problems with the way e-learning platforms generally let you manage your content:
1. If you are using a binary format like Word documents to write exos and you ship PDFs in a drive or another type of storage
   - It's not easy to highlight and present code consistently
   - You need to do PDF exports for each even tiny update
   - You can't automate some cleaning tasks like code formatting, compilation verification, linting, ...
   - When you reupload your PDF you need to inform your students of the update and ask them to download it again
   - You maintain another document with the solutions or you put them at the end, removing the benefit of colocation
1. If you usually create and deploy exos with a dedicated form on an online e-learning platform (Socrative, Moodle quizzes, ...)
   - When exos duplication is not possible, you are forced to do a lot of manual copy-pasting
   - When there is no parent-children exos architecture, the main instruction is duplicated in each sub question making harder to maintain it
   - You probably experienced that doing a lot of edits, like enhancing a sentence that you copy-pasted 20 times, or refactoring some code snippets in all exos, means you need to do the same edit 20 times and jump between 20 exos
1. In both case collaboration is harder, and students' contribution is almost impossible because either 
   - They don't access your source file, so they can propose changes themselves. Even if they could, they are still binary files, tracking changes or resolving a conflict is pretty hard.
   - Or they can't propose changes via the online form because it wasn't designed for collaboration. Even between teachers, you end up duplicating exos across accounts and some classes have access to different exos than others.

## The solution
**Delibay tries to redefine this painful and slow process that prevent continuous improvement and students' contribution.** It's important to make the course very easily and frequently updated and deployed (several times a day). Students' contribution should be easy, so they can participate by fixing formatting issues, fix confusing instructions, add missing exos, and help the teachers keeping it up-to-date to finally reach a high quality level of training that benefit everyone.

### The edition strategy
**The strategy is to manage the course via text files outside Delibay in a public Git repository**. This enables benefitting at the same time from the **power of IDEs** in terms of productivity, and **the collaboration possibilities** via Issues, Pull requests, Forks and versionning.

1. The whole course content is written in the DY syntax (using the `.dy` extension and is composed of a set of prefixes and keywords). This syntax can be highlighted and be live previewed (coming soon in VSCode).
1. The syntax is easy to learn, understand and write. It is documented on the next page. It allows describing any type of exo format and settings supported in Delibay
1. You can do dozens of repetitive edits via search and replace, duplicate group of exos. They are faster to edit (no need to jump between fields) and don't require the mouse. You can use your favourite IDE where you are already productive.
1. Solutions are maintained in the same place. No more separate document to remember to maintain.
1. No vendor lock-in: you already have all of your content outside Delibay, there is no need to export or do complicated steps to get back your exos.
1. They are always readable and editable, they don't require internet access, you are not dependant of a proprietary binary formats that might disappear in the future.
1. They allow to be edited by automations (Bash scripts or CLIs) for cleaning, generating, checking or transforming content before deployment. (i.e. You want to make sure all you code snippets are formatted and do compile).

### The deployment strategy
We have a Git repository with our all skills and exos, now comes the deployment part. **The strategy is to do automatic deployments after each push**. There is no need to configure any CI, you need to configure a webhook with a shared secret on the software forge once so it can notify the Delibay server about new commits. You git push and less than 10 seconds later it's live. Students then synchronize down and easily get all updated content.

To avoid loosing associated answers by deleting and reimporting all exos, Delibay only applies updates and create new exos in the database. To be able to uniquely identify exos that can be changed, moved around and moved to another skill, all exos need to have a local id (automatically generated) in the DY syntax. This is the little drawback of this approach compared to using online forms where changes are done directly in the database.

If the content contains errors, the parser will fail the deployment and errors would be visible on the frontend. Deletions are currently ignored to avoid loosing answers by accident. We might implement automatic archiving or manual hard deletion in the future if it is useful.

You can also run updates manually via the web interface. This can be useful if you don't want to auto deploy.

## Course repository
Your repository need to contains the following elements:
1. A `course.dy`: at the root to define the course information
1. A `skills.dy`: at the root to define the skills list
1. Some exos: placing then under an `exos` folder is recommended but is not forced, all DY files in the repository are considered.

### Managing the order and assigning numbers
The order is automatically defined from top to bottom. Skills are displayed in the same order that given in `skills.dy`. The order of exos is defined by the parser that parses files from top to bottom in the repository. The order inside a DY file is respected too. In the web interface, exos and skills are displayed in order with a dynamically generated number starting at 1. For these reasons you really don't need to assign numbers manually.

**In a nutshell, our recommendations are**
1. An `exos` folder
1. One file per parent skill to contain associated exos
1. Never hard coded numbers, let Delibay generate them for you on display

Here is an example of a basic fictive C programming course, to show you **the recommended structure**

We consider this list of skills (here the numbers are autogenerated, the names don't have any number)
1. Variables
1. Compilation
1. Logic
1. Functions
1. Arrays
1. Pointers
1. Data structures
   1. Tree
   1. Stack

with the following structure
<!-- Generation of this structure
touch README.md course.dy skills.dy
mkdir exos && cd exos
touch variables.dy compilation.dy logic.dy functions.dy arrays.dy pointers.dy data-structures.dy
cd ..
tree
-->
```
.
├── course.dy
├── exos
│   ├── arrays.dy
│   ├── compilation.dy
│   ├── data-structures.dy
│   ├── functions.dy
│   ├── logic.dy
│   ├── pointers.dy
│   └── variables.dy
├── README.md
└── skills.dy
```
This structure can be described as: An `exos` folder with one exo file per parent skill to contain all associated exos. Exos for subskills are in the same file. I.e `data-structures.dy` contains exos for the skill `Data structures`, `Tree` and `Stack`.

Why we think this structure is good:
1. You can easily change the order of skills inside `skills.dy`
1. You can have a lot of exos in only a few files

Here is an example of what we consider **a working but less maintainable structure**:

With the given skills
1. 01 Variables
1. 02 Compilation
1. 03 Logic
1. 04 Functions
1. 05 Arrays
1. 06 Pointers
1. 07 Data structures
   1. 07.01 Tree
   1. 07.02 Stack

and this structure
<!-- Generation of this structure
touch README.md course.dy skills.dy
mkdir 01-variables 02-compilation 03-logic 04-functions 05-arrays 06-pointers 07-data-structures 07.01-tree 07.02-stack
cd 01-variables/
touch 01-hello-world-fix.dy 02-primitive-types.dy 03-initialisation.dy 04-constants.dy 05-casting.dy
cd ..
cd 02-compilation/
touch 01-your-compiler.dy 02-cmake.dy 03-language-version.dy
cd ../..
-->
```
.
├── course.dy
├── 01-variables
│   ├── 01-hello-world-fix.dy
│   ├── 02-primitive-types.dy
│   ├── 03-initialisation.dy
│   ├── 04-constants.dy
│   └── 05-casting.dy
├── 02-compilation
│   ├── 01-your-compiler.dy
│   ├── 02-cmake.dy
│   └── 03-language-version.dy
├── 03-logic
├── 04-functions
├── 05-arrays
├── 06-pointers
├── 07-data-structures
│   ├── 07.01-tree
│   └── 07-02-stack
├── README.md
└── skills.dy
```

What is problematic here:
1. By creating one file per exo you need to introduce numbers to control the order and bypass the default alphabetical order
1. If you want to invert two skills, you will need to rename a several folders and rename skills in `skills.dy` because of the hard coded numbers
1. You need to choose filenames for each new exo
1. If you want to reorder exos or insert a new one at the beginning, again you'll have to rename a bunch of numbers
1. Having so much file means you are constantly switching files

<!-- is there a real argument for the exos folder ? -->