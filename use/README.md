# User guide
This user guide is an ongoing effort to help first adopters with the Delibay onboarding. The goal right now is to document core features and best practices to design practice-oriented courses.

Some features will still change in the future, this guide will be updated at the same time. To see more about brainstormed features, see their respective Concept page further down in the menu.

**The progress in redaction:**
| Page                | Abstract                                                    | Progress |
| ------------------- | ----------------------------------------------------------- | -------- |
| Getting started     | Beta usage, current project state, features in development. | 80%      |
| Manage your account | Basic administrative features to manage your account.       | 90%      |
| Manage a course     | The strategy developed to edit and deploy your courses.     | 85%      |
| DY syntax           | Learn the DY syntax core principles.                        | 60%      |
| Course and skills   | Define course and skills in DY.                             | 90%      |
| Exo formats         | Available exos formats with associated DY syntax            | 40%      |

