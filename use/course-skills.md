---
order: 7
---
# Course and skills

## Define your course
Reminder: You define your course information in a file called `course.dy`. It can contain only one course.

A course has the following properties, defined with their associated keyword:
| Property     | Prefix          | Comment                                                                                                                  |
| ------------ | --------------- | ------------------------------------------------------------------------------------------------------------------------ |
| Name         | `Course:`       | This prefix defines the start of the course block.                                                                       |
| Code         | `Code:`         | A short 2-5 letters name of your course title                                                                            |
| Goal         | `Goal:`         | The WHY of your course. What your students will be able to do at the end and what is the benefit to learn these skills.  |
| Copyright    | `Copyright:`    | Put any information on the authors and contributors of this course, possible sources and licence applied to the content. |
| Contributing | `Contributing:` | How you would like students to contribute to the course.                                                                 |

*Reminder: the prefixes are correctly extracted when given in correct order (see the order of above rows), and only below a `Course:` prefix.*

<!-- TODO: document one line versus several lines versus block starter -->
<!-- TODO: add copyright, goal and contributing fields to database ! -->

<!-- TODO: add strategy on how to properly write the goal. -->

An example with a course on regexes:
```dy
Course: Deliberate Regex

Code: DRX

Goal: The goal is to help you build a mental model allowing you to easily **read**, **edit**, **write** and **test** relatively complex regular expressions for everyday IT tasks. You will not only practice all the possible tokens via hundreds of examples, but you will understand how to find your way around the different regex engines and variants to use them in common programming languages.  
No matter where you use them, you will feel the benefit from the power of regexes: in the search bar of your IDE, in any modern language to validate or parse data, in a filter argument on `grep -P`, be productive with them at the end of this course. This is another cross-domain and cross-ecosystem tool to have in your toolbox.

Copyright: This course is made by Samuel Roland and is released under CC-BY-SA 4.0. You are free to use, improve or built upon and share this course, in conditions that you respect the license terms.

Contributing: We manage this course on [codeberg.org/samuelroland/deliberate-regex](https://codeberg.org/samuelroland/deliberate-regex), if you see any typo or you want to improve the content, we are more than welcome. If this is a trivial change, just send a PR with your proposed fix. If you want to add exos or restructure some exos, open an issue so we can discuss it before you implement something that could be not aligned with the goal of the course. We are happy to help refine your contributions before merging them because you can really help make the course more complete.
```
*Note: highlighting of the DY syntax is coming... sorry for the inconvenience in the meantime.*

## Define your skills
Reminder: You define your skills in a file called `skills.dy`.

| Property    | Keyword                | Comment                                                                                       |
| ----------- | ---------------------- | --------------------------------------------------------------------------------------------- |
| Name        | `Skill:` or `Subskill:` | `Subskill:` is only usable after a `Skill:`. It defines the start of the skill/subskill block. |
| Description | `Description:`         |                                                                                               |
<!-- TODO: document one line versus several lines versus block starter -->

A skill is part of a course and can be a subskill of another skill.

<!-- TODO: should we add a list of skills of Deliberate Regex to be coherent with the precedent example ? -->
An example with a course on C (only a few descriptions are given):
```dy
Skill: Variables
Description: Read and store any primitive kind of information, with the correct data type, and managing conversions to other similar types. You should be confident in naming, conventions and values' initialization.
Skill: Compilation
Description: Understand the steps of compilation, see and predict the preprocessor output.
Skill: Logic
Skill: Functions
Skill: Arrays
Skill: Pointers
Skill: Data structures
Description: Choose and use the appropriate data structures.
Subskill: Enums and structs
Subskill: Arrays of pointers
Subskill: Doubly linked list
```

## Browsing courses and skills
The start page you see when you are logged in is the List of courses. Currently, everyone sees all courses in their organization, there is no concept of class or groups.

## Deploy your course
**Warning: this interface and deploy system is not built yet...**

