# Rich diff of DY syntax inside forges PRs

Github does a really nice job at showing rich diff for Markdown when reviewing changes online. Currently, PRs on courses made with Delibay just have the regular Git diff view that can be hard to read when changing a few words in long lines or moving exos around...

What if we could do a rich diff for exos to easily understand the changes, with exos preview and changes highlight (like does Git Delta) !?

There are several ways to accomplish this:
1. Make a dedicated interface that can fetch information about the PR from the forge API
1. Create a web extension that could show a "DY diff" button in PRs reviews and would inject a template section in parallel to the existing text diff.

## Dedicated interface idea
TBD

## Web extension idea
with the Manifest v3 format (Firefox/Chrome)

