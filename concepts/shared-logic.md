# Shared logic
This is brainstorming about why and how we could use the `ExoShow.vue` Vue component and some parsing logic code outside the frontend codebase.

## On the website
It would be awesome to embed the ExoShow component on the intro page of `delibay.org` to show a part of the training experience so interested students and teachers can directly have a try. This would show how it feels to have an instant and/or qualitative feedback, especially with advanced exo formats and having an interactive component is better than nice screenshots.

## In the docs
To document possible exo format, it would be extremely useful to be able to show preview and transcripts, with different variants (MCQ with multiple mode ON and OFF, different table styles, etc.). This makes it testable and easier to discover and understand which format is appropriated for the next exos to import.

## In a VSCode extension
Integrating the `ImportView` or a part of it in a VSCode extension would be very helpful during course creation or transcription. It would help detect errors early, have an interactive preview, see the overall picture of the course creation progress.

## The technical side
A few questions need to be solved:
- Is it possible to mount a Vue component in several parts of a Laravel generated or static webpage without a full VueJS SPA ? Which techniques to use for that ?
  - Does it imply we need wrapper component that take a raw transcript string as argument, call parsing functions on it, and enable the switch between transcript view and exo preview ?
- How do we ship these components and logic out of the frontend and maintain them up-to-date everywhere ?
  - Is a separate NPM package a good solution ? Is it going to make the maintenance and development more complex ?
  - Does updating manually from time to time by copying the necessary files is better (with a Bash script potentially) ?
- How to easily configure the props of the `ExoShow` component in the middle of the documentation ?
  - Is it easy to write an extension to the Laravel Markdown parser to support code snippet in the transcript language to be parsed and given to the component ready for the display ?
  - Would it be possible to define other props via a piece of JSON/YAML on top of the code snippet ?
- In terms of licensing, make sure everything is compatible

