# Website
Brainstorming and documenting the features and development of `delibay.org` in its respective repos.

## Features
1. A landing page with the WHY
1. A further read to go deeper in the WHY and the HOW
1. Documentations
   1. User guide
   1. Concepts
   1. Contribute
   1. Developer guide
1. Changelog API for all codebases and interactive page
1. About page


## Content management
The website mostly consists of a lot of Markdown articles written and managed in the `project` repos. 

TODO

## Documentations
Goal: TBD
Structure: TBD

## About page
Goal: TBD

## Future things to implement
1. Fast search capability across docs and other pages
1. Changelog API
1. Changelog page with all latest versions, filters to show beta/major/minor/patch releases notes, filter by repository, search keywords or version numbers
1. 