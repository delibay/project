# Experience conception

We do design and conception of Delibay in this folder. The goal is generally to write down reflections about non-trivial features that could make the life of students and teachers easier. Then we ask teachers and students to give feedbacks about them, to make sure they are appropriate and designed in the best way.

## Current draft concepts
| Name                       | Abstract                                                                | Status                                                           |
| -------------------------- | ----------------------------------------------------------------------- | ---------------------------------------------------------------- |
| Advanced matching          |                                                                         |                                                                  |
| Best practices             |                                                                         |                                                                  |
| Fix exo format             | Exo format for code snippets to fix with per line feedback.             | Brainstorming                                                    |
| Mass import                | Massive import of content via different formats                         | Refactoring and implementing                                     |
| Mass update                | Related to import. The process of updated course content once imported. |                                                                  |
| Offline support            | How we manage everything about the local state                          | Brainstorming and testing                                        |
| Open exo format            |                                                                         |                                                                  |
| Rich intermediate feedback |                                                                         |                                                                  |
| Shared logic               |                                                                         |                                                                  |
| Sync                       | Syncing system for sync up answers, and sync down course updates.       | Implemented. Need more brainstorming <br>for other data to sync. |
| Website                    | Website architecture and content                                        | Brainstorming                                                    |

*Note: when features are implemented they will be documented and put in user docs...*
