<!-- omit from toc -->
# Mass update
**Status: brainstorm**
- [External content maintenance](#external-content-maintenance)
- [The problem](#the-problem)
- [The solution](#the-solution)
- [Files pull from a remote Git repo](#files-pull-from-a-remote-git-repo)
- [Exos research and comparison](#exos-research-and-comparison)

TODO: mass update should be named "bulk update" ?

After the **Mass import** concept, the **update step** takes to the stage. Once imported they are no way to update or fix an exo (there is no form for that). To facilitate regular update and promote continuous course improvement, this update step should be as frictionless as possible. This process would ideally run automatically or be semi-manually managed in case of special changes where teacher decision is needed.

## External content maintenance
The more I develop the import and parsing system, the more I think Delibay will maybe never have any create exo and update exo forms in Delibay because it's easier to maintain and create the content outside Delibay with transcripts. These files should be managed in a local folder and/or a Git repo, to easily make large edits (search and replace, fixing typos in group) in your favourite IDE where the large scale edits, reorganizations, code formatting, ... are easy to do.

In terms of collaboration on the course maintenance, software forges (such as GitHub, GitLab, Gitea, Forgejo, ...) are already very powerful with their issues, PRs, CI/CD, comments, webhooks, ... If the repository is public, it creates opportunities to let students contribute, fix typos, propose enhancements or new exos, ... or even teach students how to use Git and how to send pull requests by asking to do a small contribution. In my opinion, they are already the best tool to manage text files collaboratively, it would be a huge work and a big time loss to try to reimplement these features around exos management. In conclusion, I believe courses will be managed only with transcripts files.

## The problem
Let's say you have made 52 changes in you transcripts, with an update exo form you would need for every single exo to search it, edit it and save it. In addition to being tedious and time-consuming, this is a very error-prone. How do you make sure you changed all edited exos ? How do you make sure you correctly copied and pasted ?

## The solution
To have a way to import updates on a course in less than 10 seconds if they are no decision to make, or under 3 minutes to review problems and decide what to do.

The update would be divided in the following steps:
1. Updated transcript files can be pulled on the public Git repository or just be added via the standard file input like in the first import. 
1. All exos would then be parsed
1. Existing exos would be searched locally and compared
1. Created and modified exos would be detected and ready to be pushed
1. Deleted exos would be archived or deleted, this requires a human decision
1. Transcripts errors are shown at this moment if they exist
1. These changes would be uploaded via the sync up system, last undetected errors given by the server could be managed at this moment.
1. If there are no transcript error (ideally they should have been detected earlier in the VSCode extension)

## Files pull from a remote Git repo
TODO

## Exos research and comparison
TODO: that's the tough part...