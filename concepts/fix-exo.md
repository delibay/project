# Fix exo format
**Status: brainstorming :)**
TODO: find a slightly better name ?

A way to easily enable small text or code snippets to be fixed, with trivial changes on existing lines. This is an advanced exo format, which has the potential to transform some open exo to fix exo because the changes can be autocorrected line after line.

A schema for this is better than 100 words: TODO: finish and import here.

TODO:
- how to define tabs or spaces of indentation ?
  - does it matter for checking new line ? or is it just trimmed because indentation is only useful for display ? 
  - what about python where indentation could actually be something to fix ?
- how to transcript these ?
  - is it better to have a line by line thing like table ?
  - or groups like base, fixes and explanations regrouped ?

