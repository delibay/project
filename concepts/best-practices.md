# Best practices
This is **a set of best practices in draft**, written as we go along, about how to create and import content in Delibay for the most effective use. This is for students helping with transcripts and teachers who create new or transcript existing content.

## Copyright and sources
TODO

## Content restructuring
Exos in PDFs should sometimes be restructured to enable a better and more precise training. Huge exercises, with lots of details and steps with a chance to do careless mistakes, should be divided in smaller parts, to make sure the student is not completely off the track from the very start.

*Let's see concrete examples*


TODO
