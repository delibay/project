# Rich intermediate feedback
A very important feature to implement because it increases massively the quality of the feedback on long answers. There are still many ways we could solve this, the solution is not mature there are still a lot of open questions...

**TODO: expand the thinking further and ask for feedback.** 

## The problem
Let's say you are learning Bash on how to use CLI arguments...

```dy
Exo: Listing files
How could you use the `ls` command to list all files in list mode and recursively ?
Solution: ls -laR
```
Imagine you type just `ls -l` you will get `Wrong answer...` but you don't have more info on *where* you got it wrong. If you set the maximum trials to 3, you will probably lose 2 trials just to try variant like `ls -la` or `ls --la` and you will reach the limit because you have no idea if the answer is not correctly formatted or incomplete and partly or completely wrong. This solution is pretty short, let's look at a longer example:


```dy
Exo: Filtered listing
What is the command to list all files under the folder `docs`. This list should be sorted in descending order and doesn't contain starting with word `dev`.-
Solution: ls docs | sort -r | grep -vP "^dev"
```

In addition to some variants to take in account (see advanced matching concept), let's consider various student's answers and ideal feedback we would like to receive (mostly with colors and symbols):

**Correct version: `ls docs | sort -r | grep -vP "^dev"`**
1. `ls docs | grep -vP "^dev"`: all given text is correct except there is something missing after `ls docs | `
1. `ls in docs | sort | grep dev`: the `in` word is incorrect, something is missing after `sort`, same for after `grep` and around `dev`.
1. `ls docs | sort -r | grep -vP "^dev"`: 
1. `ls docs | sort -r | grep -vP "^dev"`: 

<!-- ```dy
Exo: Our SHELL
We would like to create an alias `myshell` that print this message: `Your shell is: /bin/bash` where `/bin/bash` is your shell (do not hard it obviously).
Solution: echo "Your shell is: $SHELL"
``` -->

## The solution
TODO
