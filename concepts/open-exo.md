# Open exo format
**Status: brainstorming :)**

This new exo format is targeted at open questions and small coding exercise. These kinds of exos cannot be made just with default text exo because the answer is not objective and can't be textually validated (comparing the answer string and solution string is a nonsense).

Some examples of open exo instructions with **open questions**:
1. *What is the main advantage of object-oriented programming, in comparison to procedural code ?*
1. *Can you explain step by step how to transform 1000 in binary ?*
1. *Find the biggest problem in this nginx configuration file and explain your resolution.*
1. *Can you explain in your own words what is doing the command `git merge` ?*
1. *Why the following commands do fail in this case ? How to make it work ?*

Some examples of open exo instructions with **coding exercise**:
1. *Can you please fix the function `arrangeParentsAndChildrens()` to have a lower complexity ($O(N))$ ?*, with an existing code piece. There are non-trivial changes here, possibly with architecture changes and new lines.
1. *Develop an `acronym` taking firstname and lastname as argument, and returned the acronym composed of the first firstname letter, the first and last lastname letters. Ex: Alice Hacker -> AHR.*

## Transcriptions
To enable open exo format, we just need another keyword `[open]` after the exo or subexo prefix. This would look like this:

```dy
Exo: [open] Git merge
Subexo: Why does Git merge feature exists ?
Solution: Once we are happy with our changes on our other branch (let's see `feature1`), we would like to merge it into the `main` branch (to bring the new commits on top of the `main` commits).
TODO: one more subexo
```

Using the keyword `[open]` on a parent exo means all subexos are actually open exos.
TODO: does the keyword MUST be before or after the title ?????

## Experience
What's the experience of training with these exos ?

TODO: a schema

