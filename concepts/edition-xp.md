# Edition experience

The most time-consuming part of designing deliberate courses it the edition time, the hours you spend in creating, refining and changing exos in your course.

**Creating a frictionless experience is a key to Delibay's first steps and long term usage**. The final goal would be *to enable teachers to feel confident to fix/change some exos directly in class* during small waiting times (when students are thinking about their answer i.e.)

The DY syntax alone is too raw to be really efficient. We assume teachers need something around the DY syntax to edit fast and avoid failed deployments:
1. Setup a new course easily from nothing or based on an exos documents
1. Easily do any kind of modification to existing exos and skills after first deployment: Move exos around to change its order or file, or moving it to another skill. Changing skills details and order without manually assigning numbers.
1. Have a preview of redacted exos, especially of complex parent-children exos or table exos to know how it looks. It's like Markdown, it's readable in itself but much more brain-parsable when previewed.
1. Have a minimum of colors in DY syntax, to easily differentiate prefixes and keywords, providing an additional feedback that those are particular parts and recognized in the DY syntax.
1. Catch and fix errors quickly about missing fields or incoherence due to DY syntax confusion or just omission. A reference to a non-existent skill reference
1. A way to easily insert prefixes and standard templates (like boolean exo with Exo: + Solution: true) in a fast and simple way
1. Easily find an exo in hundreds exos and dozens files to fix an error reported
1. Make sure all code snippets are formatted and are building or just syntactically correct. In addition to basic errors, having all code to be linted helps having a readable and consistent code snippet format.


<!-- 
TODO: sort these raw thoughts

## Notes about the future VSCode and Neovim extension.

## Settings VSCode style
```json
{
	"dy.formatCommands": {
		"c": "clang-format {filename} config/.clang-format",
		"cpp": "clang-format {filename}",
		"php": "pint {filename}",
	},
}
```

## Settings Lua style
TODO

## Actions VSCode style
- `Create transcript file with a duplicate of this Markdown file`
- `Format code snippets in transcriptions`: ask to commit changes before to easily discard them. use `formatCommands` described in settings.
-  -->
