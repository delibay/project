//SVG configuration to optimize SVG files before we commit and deploy them.
//Use the web extension "SVG Screenshot" (https://addons.mozilla.org/en-US/firefox/addon/svg-screenshots/)
//Copy the downloaded screenshot here (do not move the file in case the optimization break it too much 
// and you want to take back the original version)
//Then just run "npm run svgo" to run SVGO with this configuration on all images under /imgs and save in place

module.exports = {
	plugins: [
		"removeComments",
		{
			name: "removeElementsByAttr",
			params: {
				id: [],
				class: ["style-fonts"]	//Excalidraw <style> with fonts
			}
		},
		{
			name: 'addAttributesToSVGElement',
			params: {
				attributes: [
					{ "font-family": "system-ui,sans-serif" }
				]
			}
		},
		{
			name: "removeAttributesBySelector",
			params: {
				selectors: [
					{
						selector: ":not(svg)",
						attributes: "font-family"
					},
					{
						selector: "[font-stretch='100%']",
						attributes: "font-stretch"
					},
					{
						selector: "[font-weight='400']",
						attributes: "font-weight"
					},
					{
						selector: "[aria-hidden='true']",
						attributes: "aria-hidden"
					},
					{
						selector: "[text-decoration='rgb(0, 0, 0)']",
						attributes: "text-decoration"
					},
					{
						selector: "[word-spacing='0']",
						attributes: "word-spacing"
					},
					{
						selector: "[color='#000']",
						attributes: "color"
					},
					{
						selector: "*",
						attributes: [
							"aria-label",
							"aria-owns",
							"id",
							"mask"
						]
					}
				]
			}
		}
	],
};