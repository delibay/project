# Design of the logo
![the logo](logo.svg)
The logo has been made with [Inkscape](https://inkscape.org/), the working file is [logo-design.svg](logo-design.svg).

Exported versions are available:
- [logo.svg](logo.svg)
There is no PNG versions as this is not needed right now.

## Choices
Here are the choices made for the logo:
1. Font: **[Cantarell](https://cantarell.gnome.org/)** (free font released under [OFL-1.1](https://gitlab.gnome.org/GNOME/cantarell-fonts/-/blob/master/COPYING), and default font of GNOME)
1. Text: simply the word "delibay" in lowercase
1. Colors: a 3 colors gradient (positions go from 0 to 100, from left to right):
   1. Position 0: `#1aa782`
   1. Position 36: `#1ccd92`
   1. Position 100: `#20a8c3`


TODO: make a square version for favicon and other usages...