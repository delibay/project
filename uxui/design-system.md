# Design system
DISCLAIMER: this is a draft of semi-documenting how to design views and components in the frontend, it still needs a ton of thinking and refactoring before this document is really useful...

TODO: 
- build a full and solid design system for Delibay
- implement components and styles with VueJS and TailwindCSS
- refactor current design

## Reusable classes

## Options styling
*Options are possibilities in MCQ.* The default style is defined by `.option`, and the following applies when necessary:
- `option-correct` a green background for correct option
- `option-missed` an orange background for missed option
- `option-wrong` a red background for wrong option
- `option-clickable` add clickable style
- `option-selected` background colour when selected
- `option-default` default colour when the option is not selected, correct, missed, or wrong...

## Text in italic
- `text-info` a gray text as information
- `text-error` a red text for errors
- `text-success` a green text for successful operations

## Row styling
*Rows are used in the table format.*
- `row-success` a successfully filled row with all valid values
- `row-fail` a row with some invalid values

## Reusable components
### DButton
A very simple component to work as a default styled button. It can easily assign a global keyboard shortcut, push to a given route with an optional id.

Example:
```vue
<DButton @click="create()">Create</DButton>
<DButton shortcut="s" @click="trainStore.startTraining()">Start</DButton>
```

**Note: the component in globally imported, you don't need to import it in each other component.**