---
phase: MVP development
version: v0.1
version-date: 30.12.2022
---

<!-- omit from toc -->
# Design of Delibay

**This is the design document of the Delibay project made to present the project to teachers and students.** This is a long term vision with a huge number of features, only a few of them will be developed at the start and this document will frequently evolve with feedback and future reflections. Some features may not be completely adapted for teachers' and students' need. **We need a lot of feedback to enhance it!**

## Introduction

### Goal of the project
The goal of Delibay is to provide a training platform for IT students and their teachers, based on deliberate practice principles, to learn better and faster. It makes the life of teachers and students easier. Everything is built around practising skills for courses, and getting automated or human feedback on students' answers. Students can see their progress and levels in each skill, and are pushed to the limit of their comfort zone by doing harder, but accessible exercises as their level is increasing. By measuring and visualizing class progress, teachers can adapt their course to better match the needs of the class.

Delibay can be used during courses and outside and has some general features that can be used for any skills, even beyond IT, but IT students will benefit from code review, syntax highlighting, and other specific features around programming.

<div class="page">

**Table of contents**
- [Introduction](#introduction)
	- [Goal of the project](#goal-of-the-project)
	- [But what is deliberate practice ?](#but-what-is-deliberate-practice-)
	- [What is Delibay technically ?](#what-is-delibay-technically-)
	- [Dictionary](#dictionary)
- [Main features](#main-features)
	- [Managing courses and skills](#managing-courses-and-skills)
	- [Standard exercises](#standard-exercises)
	- [Dynamic exercises](#dynamic-exercises)
	- [Live exercises](#live-exercises)
	- [Easy feedback management](#easy-feedback-management)
	- [General revision mode](#general-revision-mode)
	- [Student contributions](#student-contributions)
	- [Managing projects](#managing-projects)
- [Other mockups](#other-mockups)
- [How deliberate practice concepts are applied in Delibay ?](#how-deliberate-practice-concepts-are-applied-in-delibay-)
- [*So, Delibay is the ultimate solution ?*](#so-delibay-is-the-ultimate-solution-)
- [Data model](#data-model)
- [Other points](#other-points)
- [Conclusion](#conclusion)
	- [Beta versions](#beta-versions)
	- [Disclaimer](#disclaimer)

<div class="page">

### But what is deliberate practice ?
Delibay is built to facilitate as much as possible deliberate practice principles in IT learning, as defined in the book *Peak*, written by Anders Ericsson and Robert Pool. So it's important that you have a minimal understanding of it. In the book, the starting point is that *the amount of experience and practice is not correlated to amount of expertise*. It's not sufficient to practice a skill to continue to improve. Practising over and over will just assure you are not bad at it, but when a comfortable level is reached, the level will be stable or even gradually decrease for a lot of domains. In this book, Anders Ericsson talks about nurses and doctors with several decades of experience, that were not taking better decisions for their patients that their colleagues of 3-4 years of experience. So deliberate practice is a different kind of practice with additional key components.

Deliberate practice is a set of principles that form the gold standard in terms of practice for anyone who want to achieve mastery in any field. World experts and sport champions at international levels apply those principles to benefit from the brain and body adaptability.

<!-- FIXME: add better explanations and more sources. See page 99 of the book. Add the first and the last point, and refactor. Add more quotes. -->
In brief, the principles are:
- Getting out of the comfort zone, trying constantly to progress above current abilities
- Measuring progression with objective measures and cutting down performance in small parts
- Well-defined, specific goals to improve specific parts of the performance
- It requires full attention and concentration to adjust the practice to reach the goal
- Feedback and adjustments after feedback. Teacher or coach provide feedback at the beginning, and later on, students will be able to monitor themselves.
- Improving mental representation, learned from other existing experts

A few interesting quotes from the book:

> Generally speaking, no matter what you're trying to do, you need feedback to identify exactly where and how you are falling short. Without feedback -- either from yourself or from outside observers -- you cannot figure out what you need to improve on or how close you are to achieving your goals. <br> [Page 17]

> What exactly is being changed in the brain with deliberate practice? The main thing that sets experts apart from the rest of us is that their years of practice have changed the neural circuitry in their brains to produce highly specialized mental representations, which in turn make possible the incredible memory, pattern recognition, problem solving, and other sorts of advanced abilities needed to excel in their particular specialties. <br> [Page 63]

> Initially, we had seen mental representation as being just one aspect of deliberate practice among many that we would present to the reader, but now we began to see them as a central feature -- perhaps the central feature -- of the book. The main purpose of deliberate practice is to develop effective mental representation, and, as we will discuss shortly, mental representations in turn play a key role in deliberate practice. [...]. <br> [Page 75]

### What is Delibay technically ?
Delibay consists of a web application, and a server to enable collaboration. A Delibay server should be able to host multiple schools on the same server. In case students want to use it, but their teacher is not ready/favourable, they can train in small groups, share exercises and feedback, and benefit from the tool. (This helps for the tool adoption: students can try it, use it alone or in groups, and maybe ask teachers if they are interested to use it widely).

As the server is an API, it should be easy for IT students and teachers to create other clients or web extensions to cover other needs. I hope these projects will be open source too. An example of use case for teachers would be to develop scripts that download all answers for a code exercise and run a test suite locally against each answer. IDE integrations could also be useful to do code review directly in your IDE, or submit code and fixes.

As the web application is separated from the server (not rendered in the backend) and works offline, the impact of a failure/update of the server will not completely block the learning of students but only syncing and collaborative features will be unavailable.

The client and the server will be both Free software, and that's an extremely important point because:
- Students and teachers from any school can study the code to make sure there is nothing malicious
- They can enhance the software to their needs and use
- They can reuse some code in other similar projects, just respecting the free license conditions
- They can distribute their modifications to help other students in their learning
- Even if students can't run a modified server in their school (because there is only one central server of the school), they can change the way the frontend app is working
- Teachers and students can provide feedback, make suggestions and contribute in many ways to the project
- If the authors disappear, the work will continue to exist
- IT security students can search for vulnerabilities, report them and/or patch them

### Dictionary
Just to make sure the following words are not misunderstood (because they can have different meanings):
- Exercise: a small exercise or problem containing one or several questions
- Answer: the response provided by a student on a question in an exercise
- Solution: the correct answer given by the teacher for an exercise. The solution is compared with the answer to see whether it was correct.

<div class="page">

## Main features
The models used here are only minimalist mockups with the purpose to present the interfaces. I take the fictive example of a course "PRG" (programming), to learn C++ programming.

### Managing courses and skills
![courses list page](../uxui/mockups/course-list.png)
Teachers need to define courses goals: *which skills should master students in this course ?*. After creating a course, and assigning students to it, a teacher can create the skills in a logical order and then create or import exercises inside skills.

Courses contain:
1. Different skills
1. Projects (practical work)
1. Teams

Skills contain:
1. Normal and dynamic exercises
1. Resources (documents, documentations, theory, links)
1. Eventually other sub-skills

Exercises can be divided in sub-exercises too. If you have an exercise with parts `a)`, `b)`, `c)` and `d)`, you will create 4 sub-exercises (parts).

Teachers can obviously take existing PDFs with documentations and create resources, or existing exercises and import the instructions and the solution by copy-paste. A lot of the content already exists and just need to be imported.

![skill content overview mockup](../uxui/mockups/course-details.png)
This is a student view. The course is divided in big general skills which are divided in smaller and specific skills. The top progress bar shows the general progress on the course of the student. Percentages at the right of sub-skills show the progress in the exercises, and resources. The right part is the content of the skill `Flow chart`. There are a few resources and exercises to do to acquire this little skill. The last element is an exercise without solution with a schema to create in DrawIO to practice the theory and get feedback from the teacher.

### Standard exercises
The usual workflow of a course when students work mostly on computers, is to give the whole list of exercises in a PDF, and eventually the solutions in the bottom or in another PDF... Sometimes exercises are divided in multiple PDFs or Word documents. This workflow is not optimized to have a short feedback loop. Instead of searching the solution in the solutions document or waiting a day or two to correct their answers, why not having a system that immediately says "*it's right!*" or "*it's wrong on the second value*" or even better "*it's wrong here because X*"? It would be great right ?  
When automated correction is not possible, instead of only getting the teacher solution, students could receive detailed feedback on their code, documentations, and fixes, and get advices on how to do a better work next time.

**The creation of a standard exercise with a code problem:**  
![standard exercice creation page](../uxui/mockups/exos-standard-creation.png)

**And the corresponding mockup when doing the exercise:**  
![standard exercice creation page](../uxui/mockups/exos-standard.png)

They are 2 types of standard exercises:
1. **Exercises with comparable solution**: if the solution can be matched exactly with the student's answer, the comparison can be made by Delibay (because there is no possible interpretation, it's right or wrong).
   After entering the answer, the student immediately knows if it was right or wrong. The solution can be displayed after one or more failed attempts, to have more opportunities to search the solution.

	A concrete workflow example with a basic exercise: The exercise question is "How many hours has a day ?" and the solution is "24". The question is configured to have 5 trials to the maximum.
	1. Delibay displays the question
	1. The student answers "12"
	1. The program shows that it's a wrong answer and add "12" to list of failed attempts displayed under the input <!-- TODO: adapt the mockup with this list of wrong values -->
	1. The student tries again and enter "18"
	1. The program shows again that it's a wrong answer
	1. The student finally enters "24"
	1. The program displays "Correct answer", and "2/5 failed attempts". All the attempts are stored and sent to the central server.
	1. The program goes to the next exercise

1. **Exercises without solution**: some exercises like `Explain us how does work the concept X. Write an abstract concept and give at least 2 examples.` where the answer can't be analyzed by Delibay, but need to be reviewed by a teacher and the student need to receive feedback on his answer, and eventually fix the answer.  
The exercise could optionally have a "reference solution" (the teacher solution) to show an example of a possible answer as expected by the teacher. This reference solution could be displayed after submit or only when the exercise is really done.

   A concrete workflow example of an exercise `Explain us this concept X.` with feedback of the teacher:
   1. The exercise details are displayed
   1. The student write the answer in a text area supporting Markdown (directly in the app)
   1. The student submit the answer
   1. The teacher receives the answer, review it, give feedback on specific lines or the whole text, and submit them and checks a checkbox to request fixes.
   1. The student receives feedback, fix required parts, submit the answer again.
   1. The teacher review it again, can see difference with first version and mark it as done.

   Another one will be a code exercise `Print in the console the first 200 numbers of the Fibonnaci suite`
   1. The exercise details are displayed
   1. The student opens an IDE and write the code
   1. He/she copies and pastes the code in the text area, and submit it
   1. The teacher gives feedback on specific lines of the code and submit them without asking for fixes.

	A future possibility would be to write Markdown documents or other textual files (code files) directly on the disk in a folder for the course defined by the student. For code files, this makes it easier to develop them directly in the code editor and to execute them. Then Delibay will just read the local file (or multiple files if needed) and propose to submit (no need to copy-paste at each change), and it could even show differences with the latest version.

**Exercise formats**:  
- Markdown content for nice text including eventual images
- Tables described by a JavaScript object containing cells values, and a title
- LaTeX support for math expressions

**Response formats**:  
The answer could be entered via different input types. Here are some examples of possible inputs:
- For short text: a text input
- For long text: a text area
- For true/false values: a checkbox
- For code: a file or folder

*Advanced types (not in first versions)*:
- For number values: a number input
- For fill-in-the-blank texts: a piece of text with text inputs inserted in the middle of the text. Useful when learning programming syntaxes, a piece of code with blanks can be given to fill. Useful in languages learning to train different things.
- A combination of them, like a true/false value + a textual justification: checkbox + text area.
- Code piece or file: a piece of code and an associated language can be filled, the code will be display with syntax highlighting
- Markdown document: if the exercise ask to create a longer text, the Markdown syntax can be used to support a basic layout.

### Dynamic exercises
Some exercises can be generated dynamically when needed if the question and its solution can be (randomly) generated with code (written in JavaScript). Examples: multiplication booklets, simple mental calculations, numbers conversion, finding numbers matching characteristics, ... they are a ton of use cases in mathematics, patterns detection, and thinking with basic algorithms. It's a better way to create exercises with a big (or infinite) number of questions without writing them all by hand. Let's see a simple example dynamic exercise (not particularly useful but very basic to understand how it works):

**Training multiplication booklets (between 1 and 12 here)**
1. Creating the dynamic exercise as a teacher or student
   1. Click on "New dynamic exercise"
   1. Give a title "Mental calculations" and description "Here are a few exercises to train mental calculations, from multiplication booklets to big additions".
   1. Create a first part "multiplication Booklets between 1 and 5"
   1. A small code editor appears to let the user code the part generator function. The code to write here is simply to generate 2 random numbers between 1 and 5 and return the question with the multiplication. This code is written in JavaScript, so everything available in JavaScript is available! The code runs in an isolated environment and has no access to the content of the page.
   1. The user enters `var nb1 = getRandomValue(1, 12)` and `var nb2 = getRandomValue(1, 12)` (here `getRandomValue()` is a function made available by Delibay)
   1. Then we need to return the random question with 3 keys (`instruction`: the instruction of the question, `question`: the instruction part with random values, `response`: the solution). The final code is:
       ```javascript
       var nb1 = getRandomValue(1, 12)
       var nb2 = getRandomValue(1, 12)
       return {
    		instruction: 'Calculate the following mutiplication:',
    		question: nb1 + ' · ' + nb2,
    		response: nb1 * nb2
    	}
       ```
   1. Every time the code is changing, an example question is generated and displayed. A button "Regenerate examples" can be clicked to see other random questions to make sure the code is right.
   1. The teacher now creates another part "Additions of big numbers"
   1. The code is done, the teacher clicks on "Save" and publish the exercise in the course.
1. Using the dynamic exercise as a student
   1. Students see the new published exercise
   1. They click to open it
   1. By default, there is a number of iteration of 30 for this exercise

**A mockup with a dynamic exercise to learn numbers conversion in binary, hexadecimal or decimals in all directions:**  
![dynamic exercice creation page](../uxui/mockups/exos-dynamic-creation.png)

**And the corresponding mockup when doing the exercise:**  
![dynamic exercice creation page](../uxui/mockups/exos-dynamic.png)


Now that you have a better understanding of what are dynamic exercises, let's see some real IT use cases:
1. Learning how implicit types conversions works in C++ by giving random combinations of types ([unsigned] [long|long long] int|float|char|double). This follows a specific resolution algorithm that can be developed in 20-30 lines of JavaScript and then generate dozens of pairs or code examples giving these pairs with a context.
1. Recognizing types of matrix: reflexive, symmetrical, antisymmetric, or transitive. The code would need to pick a random type, generate a random matrix that respect the constraints of the matrix type, and then ask to give the types (with 4 checkboxes for example).

*Possibilities are endless when there is a clear resolution algorithm that can be implemented, or a small list of specific cases... Dynamic exercises are great to train a small concept intensively.*

Note: a system of complexity of exercise could be designed in the future, to help students train only on questions at the limit of their level. With the multiplication booklets dynamic exercise, the code could easily calculate a complexity number from 1 to 10 by looking at the solution (something like `complexity = round((solution / max * max) * 10)` would be sufficient). Then Delibay, could start from level 1, and gradually increase the level by displaying only harder questions. If the student has difficulties on a certain level, Delibay stops to increases or go down to stabilize the level and slowly move it to the top...

### Live exercises
It's hard to involve everyone when a class is doing an exercise orally with a teacher. Only the most confident students will raise their hand to give an answer. The teacher can ask other students to answer, but a big part of students are finally not involved and do not participate. Kahoot and other platforms propose a way to make everyone participate by answering electronically, getting result on their answers, and seeing their score on the beamer in the students ranking. But we can do more than just quiz questions with multiple choices questions (like in Kahoot)...

In Delibay, to increase students engagement and active learning in a course, students can do any normal and dynamic exercises in live during the course. When exercises have a solution live statistics can be generated from the result, and for other exercises, As the teacher receive all answers, it's possible to give general advice or feedback to the class, during or after all answers have been submitted. If there is a beamer in the room, the results can be displayed and answers can be discussed. Very useful for a coding exercise with multiple solutions, some are very similar and others differ in their structure and behaviour. Giving oral feedback to everyone can help not only the author of the code but all students. This helps students to fix their mental representations of taught concepts.

Let's say students are learning JavaScript, and they need to understand a few theoretical concepts:
1. The teacher creates a new exercise asking "*Give a precise 2-3 sentences definition of the DOM, with your own words and metaphors.*"
1. The teacher creates a new exercise asking "*Where does run JavaScript?*"
1. The teacher creates a new exercise asking "*Does JavaScript can access and write files ?*"
1. The teacher select the 3 exercises, give them an amount of time, and click "Run live"
1. All students in the current class click "Join live session"
1. The first exercise instruction is displayed for every student on their screen, and on the beamer
1. Students have the given amount of time to send their answer
1. When the time has been reached or when all students have answered the question, Delibay displays all answers on all screens, so everyone can read different answers.
1. The teacher then read and comment lives the responses, giving general or specific feedback to show the errors of students. At this moment, students might be confused, and an oral discussion can happen.
1. Then, teacher clicks "Go next exercise"
1. Students do the second exercise, etc...

In another context where we want to engage students via a competition system dynamic exercises can be done as live exercises too. Let's take an example with multiplication booklets again:
1. The teacher choose the existing dynamic exercise "multiplication Booklets" and configure the minimum complexity to 7 and number of iteration to 30
1. The teacher start the session and students join it
1. The goal for students now is to answer 30 questions correctly in total (questions with a complexity level from 7 to 10)
1. When everyone has finished, the ranking of students by time is displayed, and the total time too. The total time of other classes or the last years classes could be shown too to compare. 

(There are a lot of ways we could change the workflow of live dynamic exercises to include competitiveness and gamification, but it needs some thinking).

<!-- ### Measuring progress and students level
What is boring when you study a subject, it's when you got questions about things you know perfectly instead of the things are still unsure. Dynamic exercises need to have a complexity and confidence calculation system, to determine which question is harder than the other,  -->

<!-- ### Precise feedback

TODO if relevant

on exos solutions and on human feedback on given sections of a code or document. -->

### Easy feedback management
Feedbacks sent by emails or via comments in PDFs is not necessarily a very productive system to give feedback in series as a teacher. It's better when there is a real code review system like in PRs on GitHub, but there are some pitfalls there too. Ideally, students should get feedback on projects and exercises without solution.

In Delibay teachers can review and comment exercises without solution in series. They have a list of exercises where students have sent an answer, and one after the other, teachers can inspect and give feedback on all answers. Delibay will provide an interface to add a general comment and precise comments on parts of the answer's text or specific lines of the code (like comments in Google Docs or comments in Pull Requests reviews).

![giving feedback page](../uxui/mockups/feedback-giving.png)

When reviewing answers, the teacher can decide to ask for corrections before making the exercise as done. It makes sure the student has understood its errors and know how to fix them.

*Note: in the future, to avoid surcharging teachers and making the feedback loop shorter, we could invent a system where other (advanced) students take a part of the charge and provide feedback too. Another idea would be that when a student X is coaching another student Y, Y could potentially do exercises or projects and ask X to do a code review.*

### General revision mode
As a student, when it's time to train before the exam or to refresh a skill learned a long ago, you generally read your notes, read the theory again to refresh things in your head and do some old exams and exercises to get ready. The problem is that if you read everything from the theory, you will need to read again obvious parts where you are confident instead of focusing on hard parts. Secondly, when you finished reading and took the time to understand everything, you don't really know if you have deeply integrated every skill and concepts. Sometimes, you just need to remember some concepts, but most of the time you need to be able to use in practice and see patterns and how things work.

The goal of the "general revision mode" would be to choose some exercises across all skills of a course and propose them as a way to be up-to-date again. Delibay should choose in priority complex exercises and exercises where the student failed. A certain amount of exercises or time could be picked by the students depending on their time and investment for the course. By practicing and seeing the progress and the rights answers, students have a personalized way to train and feel confident about their skills because they practiced (in assuming the exercises are good and well-designed for the course).

<!-- TODO: add a mockup, maybe the configuration part -->

### Student contributions
Sometimes, some students see errors or confusing instructions but students and teacher don't have a way to quickly fix it (or just approve a fix) and update it for everyone. With Delibay, this should become a thing of the past.  
Students should be able to propose fixes on exercises to fix errors. These fixes should be approved by the author of the exercises (here the teacher). Students can create their own exercises too. All exercises have different state (draft, released, approved). A draft exercise is only visible to the author, a released exercise is visible to the author and the teacher of the course, and only when it is approved, the exercise is visible to everyone in the school. Exercises created by students should be approved by a teacher to make sure they do not contain mistakes or if they need fixes before being approved. Students should be able to contribute to resources too, like adding a resource link, adding or fixing a section in a page, ...

The fact that students can contribute is a great way to enhance the course over the years, making it more qualitative. When students need to manage some special kind of problems, and they do not have any exercise to train this concept, they can learn it, create a few exercises, get them approved, and the whole school can benefit from this addition. The effort of improvement is shared to more people and the students become also "responsible" for the course quality.

To avoid waiting on teachers responses to approve contributions, we could invent an approval system: when 2-3 students is equivalent to a teacher, and a new exercise can be approved.

<!-- TODO: add a mockup, maybe the approval system or the change proposition interface -->

### Managing projects

Projects and practical works realized during a few days or weeks, makes a course concrete, fun and interesting. This is another type of practice that theory exercises, and Delibay is here to help manage them and provide feedback too.

**Here is a mockup for the list of projects...**  
![projects list page](../uxui/mockups/projects-list.png)

**... and another for the project details**  
![project details page](../uxui/mockups/projects-details.png)

## Other mockups
Here are other mockups not shown above because they are more "administrative" features.

**Login page:**  
![login page](../uxui/mockups/account-login.png)

**Register page:**  
![register page](../uxui/mockups/account-register.png)

**Course creation page (teachers only):**  
![Course creation page](../uxui/mockups/course-creation.png)

**Skill creation page (teachers only):**  
![Skill creation page](../uxui/mockups/course-new-skill.png)

---

## How deliberate practice concepts are applied in Delibay ?
If you are not familiar with deliberate practice, or you don't see where the key features try to integrate these concepts, let me explicit them for you:
- **A lot of practice**: everything is turning around skills and exercises, not knowledge and theory
- **A lot of feedback**: automatic and fast feedback for exercises with solutions, and a simplified way to give precise feedback for teachers, on exercises and projects.
- **Pushing students out of their comfort zone**: by choosing exercises at the limit of their level and doing old exercises where students have failed or weren't confident.

## *So, Delibay is the ultimate solution ?*
Yes, definitely... just kidding. **Obviously not**: the quality of course and exercises will not be magically good, teachers will not become great just because they use Delibay. *It's a tool not a magic wand*, it will not replace student effort, and teacher work. They need to understand how does deliberate practice work, and change their studying/teaching habits. Delibay is just here to make everything easier, but does not guarantee a result or progress in learning.

## Data model
After describing the features, here is a more technical viewpoint with the data model for the database of Delibay. It can help to understand how everything is grouped together.

![LDM](../database/LDM.png)

To know more about the database design, see [database/db.md](../database/db.md).

## Other points
1. Delibay is a frontend SPA, no need to reload the page to visit another page. This enables a fast browsing of courses items, zero waiting time before receiving automated feedback on the given answer, and a dedicated window on the computer.
1. There is no limit on the size of the local cache. The access to files could be used to download updated versions of documents, browsing the local folder of the course.
1. Ideally, Delibay should be usable entirely from the keyboard to provide ways to quickly find a specific exercise, a resource, or switch to another course.

## Conclusion
### Beta versions
The platform is going to be in beta during a long time as the development is slow outside of holidays. The first beta versions should enable students to start training with simple exercises currently in PDFs or stored on Socrative... 

My first thought was to develop a desktop application with ElectronJS but for the moment it's faster to just develop a web application that works mostly offline and not useful at all for the very start. Doing an PWA and/or Tauri app is another possibility...

### Disclaimer
I would like to end this document with a little disclaimer concerning the project. This is a huge project that is going to take at least one year to implement entirely. Currently, I want to invest the next 3 years (2023-2025) in this project because I deeply believe in its potential. After my Bachelor, I have no idea what will be the state of my motivation, and my attraction towards other projects.  
I'm going to work during my free time and I don't know how fast the project will progress, the major versions will probably happen during holidays. I will try to release often to deploy fixes and regularly add new enhancements. An advantage of this slow development mode, we will have the time to observe and understand what are the most adapted to students and teachers.

To focus myself on this project, I put aside other personal projects, but during the journey I will learn a lot on programming, testing, project management, servers administration, documentation, learning, teaching, deliberate practice, design, releases, Free software maintenance, and other interesting skills. This long design document is a good first step in this project, and it's far from being the last :)

<!-- 
## License and credits
TODO
-->
