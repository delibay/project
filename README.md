# The delibay project

**Management of the delibay project, bugs, features, conception, models, and documentations.**

## In this repository
**Folders related to deployed documentations:**
- [use](use/): usage guide (in construction)
- [concepts](concepts/): future features conception (always changing)
- [contrib](contrib/): contribution guide (almost empty and irrelevant for now)
- [dev](dev/): developer guide (in draft)

**Others parts**
- [logo](uxui/logo/logo-design.md): logo files
- [mockups](uxui/mockups/): all mockups of the project
- [database](database/db.md): database conception, schemas and documentation
- [design.md](design/design.md): the conception document

## Project repositories
- [delibay/project](https://codeberg.org/delibay/project): everything that concerns the whole project, not a specific repository
- [delibay/delibay.org](https://codeberg.org/delibay/delibay.org): the website of the project `delibay.org`
- [delibay/frontend](https://codeberg.org/delibay/frontend): the frontend application written in VueJS
- [delibay/backend](https://codeberg.org/delibay/backend): the backend application (an API) written in Laravel

## Contributions
The project is currently not opened to contributions as everything is constantly moving and changing. If you want to contribute, this is great ! and contributions will be possible in the future... for the moment, just try it, use it, give feedback on your experience and tell your friends about it.

## Licence and credits
TODO. No license for now...