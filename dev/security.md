# Security
A few notes about how Delibay handles security. TODO: continue this.

## Password storage
Based on [OWASP recommendation](https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html), we use Argon2id hashing. In [`hashing.php`](https://codeberg.org/delibay/backend/src/commit/59c5ffb75b87953af787c1dcb3098f98e2b56303/config/hashing.php#L46) we kept the default Laravel values:
```php
'argon' => [
	'memory' => 65536,
	'threads' => 1,
	'time' => 4,
],
```
