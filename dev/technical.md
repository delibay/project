# Technical documentation
**This document is a COMPLETE DRAFT and will be cleaned up later... I'm just putting down some unorganized notes for later**.

Here is a first - always WIP - version of a technical documentation, written by chunks during the MVP development. Everything will be made nice after a first major version to facilitate contribution and long term maintenance.

## Technologies
Some choices are temporary because the MVP is a rush phase without much thinking about the tools, it should just work in the start, and we'll refactor or change later.
- Laravel
- VueJS
- TailwindCSS
- markdown-it: Markdown parser in JS
- highlight.js: code highlighting JS library (may change in the future)
- Pinia: state management system

TODO: add and fix code highlighter + markdown parser + mermaid support
TODO: markdown styling + css classes and colors available for a future Delibay theme

## Database
### Testing data
You can seed your local development database with fake data. These data are defined in `DatabaseSeeder.php` and factories for each model class.
```
php artisan db:seed
```

## Strange things
Database tables and Laravel models sometimes have strange names, I chose a short version of the long names for simplicity and productivity.

Here is the equivalent:
- `Exo`: Exercise
- `Org`: Organization (school, university, ...)

## Register
The register process is divided in 4 steps:
- 1. The user give the server URL, the server is pinged to make sure this is a Delibay server
- 2. The user reads and accept the server rules
- 3. The user fills the form
	- The user clicks Submit
	- Some checks are made on the inputs and the data is sent 
	- Delibay shows request errors or the server response
- 4. As the account has been created, Delibay shows the email validation page
	- The user enters the secret code received by email
	- If invalid let the user retry
	- If valid show a successful message and login the user

## Frontend
Here is all information related to the frontend app (the repos is [`delibay/frontend`](https://codeberg.org/delibay/frontend) on Codeberg).

### Stack
The frontend is a VueJS SPA, with Vue 3 with TypeScript. The project has been scaffold with [Vite](https://vitejs.dev). Here the different tools used for development.
- [Pinia](https://pinia.vuejs.org): the official state management system of the Vue ecosystem (its predecessor was Vuex)
- [pinia-plugin-persistedstate](https://prazdevs.github.io/pinia-plugin-persistedstate/guide/why.html): a NPM package to persist and rehydrate Pinia stores in localStorage easily
- [DexieJS](https://dexie.org/): an intuitive IndexedDB wrapper, provide a SQL-like interface to manage the local state.

### Testing
Having strong and well-written tests is very important to facilitate the maintenance, refactoring, contributions, and general quality. Every contribution touching the logic should add or change automated tests. Here are the tools currently used in the project:
- [Cypress](https://www.cypress.io/): the end-to-end testing framework

<!-- - todo: unit testing framework
- todo: what do we test or not
- todo: test strategy for mocks, api calls, stores, ... 
- todo: add "how to choice the test types"
- -->


### Cypress
<!-- - todo: test location
- todo: ways to run tests
- todo: how to learn cypress (docs + courses)
- todo: add a test example and explanation
- todo: add a screenshots of tests running
- -->

## Conventions
- [Typescript](https://www.typescriptlang.org/): we use TypeScript everywhere.
- [Vue Composition API](https://vuejs.org/guide/extras/composition-api-faq.html): we use the *Composition API* instead of the *Options API* (to group things by feature and having the benefit of better type inference)
- [Script setup syntax](https://vuejs.org/api/sfc-script-setup.html): inside our single file components, the `script` tag must be `<script setup lang="ts">...</script>`. Read more on the official documentation if needed.

<!-- Naming conventions, see  -->
<!-- tabs vs space ? -->
<!-- what about the linter and formatter ? -->
---

## How to contribute
**THIS IS A DRAFT... Before accepting contributions I need to choose some free licenses.**
1. Choose a way you want to contribute
	1. Talk to your IT friends and IT teachers about the project
	1. (long term) Help to translate the project
	1. (coming soon, need some development) Report feedback during usage of the frontend, give feedback on the documentation, or the design document
	1. Report bugs via the issues
	1. Contribute to fix the existing issues with code
	1. Help us achieve a greater code quality level, by adding or fixing tests on the frontend and backend
	1. Contribute to the design

Contribute with code:
1. Setup a local installation (see README)
1. Configure your tools (linter)
1. Pick an issue
1. Fork and clone the repos of your choice
