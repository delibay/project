# Contribution guide
If you want to help to redefine the IT learning experience in your school and develop Delibay, you are more than welcome ! There are numerous ways to help with feedbacks, docs, brainstorm or code.

**Sadly, *for the moment* the project is not opened to contributions for the following reasons:**
1. They are still a lot of things to renames (files, functions, temporary feature names), test and refactor everywhere in the code, this would need to be done before accepting external contributions to make sure we don't create too much Git conflicts all the time. These future enhancements will help bigger contributions to be done and be qualitative.
1. I'm not sure how we should manage indentation in the project (tabs, spaces, how much of them) and we could easily format everything and checks that it is formatted on each PR
1. I don't know about if we should use a DCO and CLA with contributions, I still need to read, learn and decide on this.
1. It would be good to define Issue and PR templates on Codeberg
1. A lot of documentation on the technical architecture, how to write tests, 
1. Delibay is going to be a fully Free and Open-source software, but as you saw there is no licence files in the repository right now, except for the website because I just didn't choose a Free licence yet. The licences are necessary to accept contributions under them, because otherwise all rights on the contributions remain on their original authors (so we can not easily licence it later).

I'm hoping to resolve these problems soon, but soon could mean several months actually because I'm very busy with the school and the current development to start beta usage...

## In the meantime
### If you are a student
1. Talks to your friends and classmates about it
1. Talk to your teachers whether they would be interested in increasing the amount of practice and feedback in their courses.
1. Learn the DY syntax and start writing transcriptions for your courses at your school that have some existing training content.
1. Give feedbacks about the existing features or the concepts being developed at the moment (see them in the menu)

### If you are a teacher
Delibay is currently mostly built on the student's side, because this is the core value: deliberate IT training for students. Teacher features such as live trainings, advanced course organization, administration will come after the maturation of the training features.

1. Learn more about deliberate practice, i.e. by reading the book Peak. These 300 pages are so condensed and rich you will not regret it if the subject interests you.
1. Start writing transcriptions for existing or new course content
1. Give feedbacks about teacher related features, we really need to improve these parts too
1. Give feedbacks on the brainstormed concepts (see them in the menu)

## Future contribution types
Just so you know how you could contribute in the future when things will be ready:
1. **Helping with designing concepts**, they are usually a lot of open questions or remarks to do to improve them before they are developed
1. Helping with writing new articles or improving existing parts in the **documentations**
1. **Searching and reporting bugs** in the frontend and backend
1. Help with **other subprojects** like the VSCode extension or the website
1. **Fixing bugs** and **developing features** on the frontend in VueJS and the backend in Laravel
1. Writing or updating **translations** in your language

