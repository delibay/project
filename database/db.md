# Database design
This is a WIP (Work In Progress) document designing the database. A lot of changes will be done before a first stable version.

## Logical data model
![LDM](LDM-svg.svg)

## Special fields
Not every field is trivial, here are a few details about them:
- **Users**:
  - `role`: the role can take different values, 1=student, 2=teacher, and 3=admin
- **Skills**:
  - `parent_id`: skills can have sub-skills, so we have this nullable foreign key that define the parent skill for any sub-skill 
- **Exercises**:
  - `type`: exercises can have a specific type depending on the structure, the sub-exercises and the dynamicness. See more below. 
  - `solution`: a JSON piece describing the structure and the value of the solution to the exercise. See more on solution structures below.
  - `parent_id`: exercises can have sub-exercises (different parts) so we have this nullable foreign key that define the parent exercise for any sub-exercise 
- **Answers**:
  - `content`: a JSON piece describing the content of the answer 
  - `result`: a True/False value to define whether the answer was correct or not

## Storing answers
For exercise with solution, the answer content will be stored only if the answer was wrong. There is no need to store if the answer was correct as the solution is in the `exo.solution`.

For exercise without solution, the answer content is obviously stored, so another human can make feedback on it.

## Format and solution
The solution MUST only contain checking information used to validate an answer. If the solution is not present, the exercise MUST still be doable and the answer sent, with information present outside of this field. This enables to have exercises with hidden solutions (not exposed to students) if this is a need in the future.

The challenge is to support several formats and future possibilities of exercise formats. To support standard exercise, with or without solution, and dynamic exercise. The JSON format for the content saved in the answer database entry must be defined too.

## Exo formats

As only failed answers are stored, `answer.content` are examples of wrong answers. Dynamic exercises are not included because the exos field `format` and `solution` are null because they are generated with the `generator` code. As a reminder, standard exo without solution are meant to get feedback by a human. Values are shown in JavaScript object formats for the sake of simplicity.

It seems that to cover most use-cases, exercise should have 3 fields (including customizable JSON fields with specific or common properties, defined along the creation of new formats)
1. `format`: `text` a short text field giving a string as the format (such as `text`, `mcq`, ...)
1. `structure`: `null|JSON` a JSON piece to indicate how the exercise should be displayed and how to student will enter the answer. Is null for exercise without complex structure (such as text or bool).
1. `solution`: `null|JSON` a JSON piece to store the solution values. Is null for exercise without solution.

**Available formats:**
1. **Simple text**  
	1. Standard exo - given solution  
   		Example: *"what is the current major C++ version ?"* -> solution: 23 or 24
		1. `exo.format` = `"text"`
		1. `exo.structure` = `null`
		1. `exo.solution` = `{ values: ["23", "24"] }`
		1. `answer.content` = `{ value: "23" }`
	1. Standard exo - no solution  
   		Example: *"What is C++ used for ?"* -> no solution
		1. `exo.format` = `"text"`
		1. `exo.structure` = `null`
		1. `exo.solution` = `null`
		1. `answer.content` = `{ value: "For desktop and embedded applications mostly." }`
1. **Number**  
	1. Standard exo - given solution  
   		Example: *"What is the birth year of the C++?"* -> solution: 1985
		1. `exo.format` = `"number"`
		1. `exo.structure` = `{ min: 1800, max: 2022 }`
		1. `exo.solution` = `{ values: [1985] }`
		1. `answer.content` = `{ value: 1934 }`
	1. Standard exo - no solution  
   		Probably doesn't make sense.
1. **True/False question**  
	1. Standard exo - given solution  
   		Example: *"Is C++ a compiled language ?"* -> solution: true
		1. `exo.format` = `"bool"`
		1. `exo.structure` = `null`
		1. `exo.solution` = `{ value: true }`
		1. `answer.content` = `{ value: false }`
	1. Standard exo - no solution  
   		This is forbidden as this is nonsense.
1. **Multiple Choice Questions with one or multiple options**  
	1. Standard exo - given solution  
		Example: *"Check all pets among the following animals:"*, with options "cat", "dog", "cow", "chimp" -> solution: "cat" and "dog"
		1. `exo.format` = `"mcq"`
		1. `exo.structure` = `{ options: ["cat", "dog", "cow", "chimp"], multiple: true }`
		1. `exo.solution` = `{ values: [0, 1] }`
		1. `answer.content` = `{ values: [0, 1, 2] }`
		1. TODO: rename values: to options: ?? or indexes ?
	1. Standard exo - no solution  
		This is forbidden as this is nonsense.
<!-- TODO: add cases for multiple = false, solution.values.length > 1 or == 1 -->
1. **Table to fill**  
	1. Standard exo - given solution  
   		Example: *"Fill this truth table for the OR gate"* -> solution: a few table inputs
		1. `exo.format` = `"table"`
		1. `exo.structure`:
			```js
			{
				height: 4,	//the width in case they are empty cells at the end
				width: 3,	//the height in case they are empty cells at right
				header: 1,	//the number of lines to count as header
				type: "number",	//define the default type of cell (used for empty rows too)
				style: {
					//possible style on rows identified by their ID and on specific cells.
				}
				//define the rows and cells with fixed cells and cells to fill
				rows: {
					//Rows are indexed with a unique ID (starting at 1) that could be reused in another attribute later
					0: ["A", "B", "A+B"],
					1: [0, 0, {id:1}],
					2: [0, 1, {id:2}],
					3: [1, 0, {id:3}],
					4: [1, 1, {id:4}],
				}
			}
			```
         1. `exo.solution`: the keys in `values` are the cell ids defined above
			```js
			{
				values: {
					1: 0,
					2: 1,
					3: 1,
					4: 1,
				}
			}
			```
		1. `answer.correct`: Question: is `true` only if all rows are correct ? Question: we store only one answer for the whole table ?
		1. `answer.content`: the keys in `values` are the cell ids defined above. **Only wrong values are stored!**
			```js
			{
				values: {
					1: 1,
					2: 0,
					//Last 2 cells were correct and were not stored
				}
			}
			```
		**Here is another example with a table with one row per iteration of a bubble sort algorithm:**  
		```js
		{
			height: 5,
			width: 4,
			header: 1,
			rows: {
				0: [4, 3, 1, 2],
				1: [{id:1},{id:2},{id:3},{id:4}],
				2: [{id:5},{id:6},{id:7},{id:8}],
				//There is still 3 empty rows displayed without any cells to fill, 
				//but those are not stored because this is useless
			}
		}
		```
		1. `exo.solution`
			```js
			{
				values: {
					//Row 1
					1: 3,
					2: 1,
					3: 2,
					4: 4,
					//Row 2
					5: 1,
					6: 2,
					7: 3,
					8: 4,
				}
			}
		1. `answer.content` Question: should we store a list of boolean values for the correctness of each line ?
			```js
			{
				values: {
					1: 1,
					2: 3,
					5: 2,
				}
			}
			```
	1. Standard exo - no solution  
   		Example: *"Explain in this table 3 concepts behind OOP, and their advantages compared to functional programming."* -> no solution
		1. `exo.format` = `"table"`
		1. `exo.structure`
			```js
			{
				height: 4,	//really useful
				width: 2,	//idem
				rows: [
					"Concept name", "Advantages"
					{ id: 1, type: "text" }, { id: 2, type: "text" },
					{ id: 3, type: "text" }, { id: 4, type: "text" },
					{ id: 5, type: "text" }, { id: 5, type: "text" },
				]
			}
			```
		1. `exo.solution` = `null`
		1. `answer.content`
			```js
			{
				values: {
					0: "Polymorphism",
					1: "It makes object great",
					2: "Encapsulation",
					3: "Making object self-contained, this helps to group related logic.",
					3: "Blabla",
					...
				}
			}
			```
1. **For fill-in-the-blank texts**
    1. Standard exo - given solution  
		Example: *"Fill the blanks in the following sentences"* -> a few words to enter
		1. `exo.format` = `"fill"`	//better name ?
		1. `exo.structure` = `{ text: "The C++ is a {0} language developped in {1}" }`
		1. `exo.solution` = `{ values: { 0: "compiled", 1: "1985" }`
		1. `answer.content` = `{ values: { 0: "interpreted", 1: "1950" }`

		Example 2: *"Complete this C++ snippet to print the lastname only"* -> a short piece of code
		1. `exo.format` = `"fill"`	//better name ?
		1. `exo.structure` = `{ text: 'string fullname = "Jone Dear"; cout << {0};' }`
		1. `exo.solution`
			```js
			{ 
				values: { 
					0: [
						'fullname.substr(0, fullname.find(" "))',
						'fullname.substr(0, fullname.find(" ") - 1)',
						'string(fullname, 0, fullname.find(" ") - 1)',
					]
				}
			}
			```
		1. `answer.content` = `{ values: { 0: 'fullname.substr(0, " ")', 1: "1950" }`
	1. Standard exo - no solution  
   		Not possible, probably doesn't make sense
1. **File to create**
	1. Standard exo - given solution  
		Not possible
	1. Standard exo - no solution  
   		Example: *"Create a flow chart in DrawIO of the tic-tac-toe game"* -> no solution
		1. `exo.format` = `"file"`
		1. `exo.solution` = `null`
		1. `answer.content` = `"234jkl234j23lh62345j2l34j23l42j.jpeg"` (a file name)

### Exercises validation rules
As we store different exercises types in the same table, some values are required for some types, and others are just null. This logic is not part of the database but let's define what need to be checked in the backend to validate information of the exercises, depending on their types:
- TODO: !

## Dynamic exercises structure
TODO: !

## Brainstorming
A list of undecided points that need consideration...
- which exercises types should exist ?
- which answers states should exist ?
- what are values for answers.confidence ? how the system really works behind it?
- should answer type be stored ? duplicated of exercises.solution_type
- add the given question with the answer on a dynamic exercise ?
- add answer
- shoud answers.result be called answers.correct ?

## Missing use cases
The schema requires changes for these use cases that do not fit into the current schema:
- students release something for a project
- students send a file for an exercise
- teachers provide a feedback on answers and project releases
- students are attributed to different classes and virtual classes for courses
- teachers give students a bunch of exercises as homework
- school participation should be moderated ?
- organisations should have administrators ?